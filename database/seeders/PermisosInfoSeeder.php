<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;
use App\Models\AddyEstado   AS Estados;
use App\Models\AddyAccione  AS Acciones;
use App\Permisos\Models\Permission AS Permisos;
use App\Permisos\Models\Role;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;

class PermisosInfoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //truncate tables
        DB::statement("SET foreign_key_checks=0");
            DB::table('role_user')->truncate();
            DB::table('permission_role')->truncate();
            Permisos::truncate();
            Role::truncate();
        DB::statement("SET foreign_key_checks=1");

        //user admin
        $usuario_administrador= User::where('email','admin@admin.com')->first();
        if ($usuario_administrador) {
            $usuario_administrador->delete();
        }

        $usuario_administrador= User::create([
            'name'          => 'Administrador',
            'email'         => 'admin@admin.com',
            'password'      => Hash::make('admin'),
            'id_empresa'    => '1',
            'estado_user'   => '1'
        ]);

        //rol admin
        $rol_administrador=Role::create([
            'nombre' => 'Administrador',
            'slug' => 'administrator',
            'descripcion' => 'Super usuario del sistema Addy',
            'full-access' => 'yes'
    
        ]);

        //rol agente
        Role::create([
            'nombre' => 'agentes',
            'slug' => 'agentes',
            'descripcion' => 'Agentes',
            'full-access' => 'no'
        ]);

        DB::table('tipo_identificions')->insert([
            'nombre_tipo'   => 'Cédula ciudadania',
            'estado_tipo'   =>  '1',
        ]);

        DB::table('tipo_identificions')->insert([
            'nombre_tipo'   => 'NIT',
            'estado_tipo'   =>  '1',
        ]);

        DB::table('tipo_identificions')->insert([
            'nombre_tipo'   => 'Cédula extranjeria',
            'estado_tipo'   =>  '1',
        ]);

        Estados::create([
            'nombre_estado'         => 'Activo',
            'descripcion_estado'    => 'Activo en el sistema',
            'modulo_estado'         => '0',
        ]);

        Estados::create([
            'nombre_estado'         => 'Inactivo',
            'descripcion_estado'    => 'Inactivo/Eliminado en el sistema',
            'modulo_estado'         => '0',
        ]);
        
        Estados::create([
            'nombre_estado'         => 'Perfil Registrado - Incompleto',
            'descripcion_estado'    => 'Perfil registrado pero aun no llena el formulario',
            'modulo_estado'         => 'usuarios',
        ]);

        Estados::create([
            'nombre_estado'         => 'Bitacora Iniciada',
            'descripcion_estado'    => 'Bitacora iniciada',
            'modulo_estado'         => 'bitacoras',
        ]);

        Estados::create([
            'nombre_estado'         => 'Bitacora en Proceso',
            'descripcion_estado'    => 'Bitacora iniciada',
            'modulo_estado'         => 'bitacoras',
        ]);

        Estados::create([
            'nombre_estado'         => 'Bitacora terminada',
            'descripcion_estado'    => 'Bitacora se ha terminado',
            'modulo_estado'         => 'bitacoras',
        ]);

        Acciones::create([
            'codigo_accion'         => '0',
            'descripcion_accion'    => 'Sin acción previa',
            'modulo_accion'         => 'bitacoras',
            'id_estado_accion'      => '1',
        ]);

        Acciones::create([
            'codigo_accion'         => '1',
            'descripcion_accion'    => 'Registrar llamada',
            'modulo_accion'         => 'bitacoras',
            'id_estado_accion'      => '1',
        ]);        

        Acciones::create([
            'codigo_accion'         => '2',
            'descripcion_accion'    => 'Registrar cita programada',
            'modulo_accion'         => 'bitacoras',
            'id_estado_accion'      => '1',
        ]);

        Acciones::create([
            'codigo_accion'         => '3',
            'descripcion_accion'    => 'Registrar presentación de compradores',
            'modulo_accion'         => 'bitacoras',
            'id_estado_accion'      => '1',
        ]);

        Acciones::create([
            'codigo_accion'         => '4',
            'descripcion_accion'    => 'Registrar un toque',
            'modulo_accion'         => 'bitacoras',
            'id_estado_accion'      => '1',
        ]);

        DB::table('addy_detalle_roles_empresas')->insert([
            'empresa_id'    => '1',
            'role_id'       =>  '2',
        ]);

        //table role_user
        $usuario_administrador->roles()->sync([ $rol_administrador->id ]);

        //Permisos
        $permisos_all = [];
        
        // ------ * Modulo de Permisos * ------ //
  
            $permisos = Permisos::create([
                'name'        => 'Vista Principal Permisos',
                'slug'          => 'permisos.principal',
                'description'   => 'El usuario podrá ver en una lista todos los permisos del Sistema.',
                'name_module'         => 'permisos',
            ]);
            $permisos_all[] = $permisos->id;
            

            $permisos = Permisos::create([
                'name'        => 'Crear Permiso',
                'slug'          => 'permisos.crear',
                'description'   => 'El usuario podrá crear un Permisos en el Sistema.',
                'name_module'         => 'permisos',
            ]);
            $permisos_all[] = $permisos->id;


            $permisos = Permisos::create([
                'name' => 'ver información de Permiso',
                'slug' => 'permisos.ver',
                'description' => 'El usuario podrá ver modal información del permiso seleccionado.',
                'name_module' => 'permisos',
            ]);
            $permisos_all[] = $permisos->id;
                    
                    
            $permisos = Permisos::create([
                'name' => 'Edición de Permisos',
                'slug' => 'permisos.editar',
                'description' => 'El usuario podrá editar en una modal información del permiso seleccionado.',
                'name_module' => 'permisos',
            ]);
            $permisos_all[] = $permisos->id;
            

            $permisos = Permisos::create([
                'name' => 'Eliminar un Permiso',
                'slug' => 'permisos.eliminar',
                'description' => 'El usuario podrá eliminar un permiso seleccionado.',
                'name_module' => 'permisos',
            ]);
            $permisos_all[] = $permisos->id;
        // ------ * Modulo de Permisos * ------ //

        
    }
}
