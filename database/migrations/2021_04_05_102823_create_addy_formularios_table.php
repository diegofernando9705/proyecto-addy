<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAddyFormulariosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('addy_formularios', function (Blueprint $table) {
            $table->id();
            $table->longText('pregunta');
            $table->longText('respuesta');
            $table->char('fecha_respuesta', 30);
            $table->integer('cedula_persona')->length(12)->unsigned()->nullable(false);
            $table->longText('tipo_cliente');
            $table->integer('estado_formulario')->length(12)->unsigned()->nullable(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('addy_formularios');
    }
}
