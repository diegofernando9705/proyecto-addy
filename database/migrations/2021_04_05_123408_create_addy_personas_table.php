<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAddyPersonasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('addy_personas', function (Blueprint $table) {
            $table->integer('cedula_persona')->primary();
            $table->integer('tipo_identificacion')->length(12)->unsigned()->nullable(false);
            $table->longText('foto_persona');
            $table->char('nombres_persona', 50);
            $table->char('apellidos_persona', 50);
            $table->char('fecha_nacimiento', 50);
            $table->longText('correo_persona');
            $table->char('telefono_fijo', 10)->nullable();
            $table->char('celular_movil', 15)->nullable();
            $table->char('celular_whatsapp', 15);
            $table->longText('direccion_persona')->nullable();
            $table->integer('codigo_ciudad')->length(12)->unsigned()->nullable(false);
            $table->char('codigo_pais', 10)->nullable(false);
            $table->integer('codigo_postal')->length(12)->unsigned()->nullable();
            $table->char('nit_empresa');
            $table->integer('id_perfil')->length(12)->unsigned()->nullable(false);
            $table->integer('id_usuario')->length(12)->unsigned()->nullable(false);
            $table->integer('estado_persona')->length(12)->unsigned()->nullable(false);
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('addy_personas');
    }
}
