<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAddyHistorialesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('addy_historiales', function (Blueprint $table) {
            $table->id();
            $table->longText('descripcion_historial');
            $table->longText('codigo_bitacora');
            $table->integer('id_usuario')->length(12)->unsigned()->nullable(false);
            $table->longText('fecha_historial');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('addy_historiales');
    }
}
