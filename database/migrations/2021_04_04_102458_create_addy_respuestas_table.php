<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAddyRespuestasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('addy_respuestas', function (Blueprint $table) {
            $table->id();
            $table->longText('descripcion_respuesta');
            $table->integer('id_pregunta')->length(12)->unsigned()->nullable(false);
            $table->integer('id_formulario')->length(12)->unsigned()->nullable();
            $table->char('fecha_respuesta', 20);
            $table->integer('estado_respuesta')->length(12)->unsigned()->nullable(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('addy_respuestas');
    }
}
