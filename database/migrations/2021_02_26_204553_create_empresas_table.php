<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmpresasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('empresas', function (Blueprint $table) {
            $table->id();
            $table->longText('logo_empresa')->nullable();
            $table->longText('favicon_empresa');
            $table->integer('code_tipo_documento');
            $table->integer('indentificacion_tributaria')->unique();
            $table->char('nombre_empresa', 150);
            $table->char('email_empresa', 150);
            $table->char('descripcion_empresa', 150)->nullable();
            $table->char('telefono_principal', 15)->nullable();
            $table->longText('telefono_whatsapp')->unique();
            $table->longText('palabras_claves')->nullable();
            $table->longText('acerca_de_negocio')->nullable();
            $table->char('codigo_pais', 5);
            $table->char('codigo_departamento', 5)->nullable();
            $table->char('codigo_ciudad', 10);
            $table->char('codigo_postal', 150)->nullable();
            $table->char('direccion_empresa', 150)->nullable();
            $table->integer('estado_empresa');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('empresas');
    }
}
