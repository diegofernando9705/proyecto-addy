<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAddyPreguntasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('addy_preguntas', function (Blueprint $table) {
            $table->id();
            $table->longText('descripcion_preguntas');
            $table->char('id_modulo', 30);
            $table->char('id_opcional', 50)->nullable();
            $table->integer('id_empresa')->length(12)->unsigned()->nullable(false);
            $table->char('fecha_pregunta', 20);
            $table->char('nombre_usuario', 20);
            $table->integer('estado_pregunta')->length(12)->unsigned()->nullable(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('addy_preguntas');
    }
}
