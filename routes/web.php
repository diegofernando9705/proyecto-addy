<?php

use Illuminate\Support\Facades\Route;
use Laravel\Socialite\Facades\Socialite;
use App\Http\Controllers\PermisosController 	AS Permisos;
use App\Http\Controllers\EmpresasController 	AS Empresas;
use App\Http\Controllers\ModulosController 		AS Modulos;
use App\Http\Controllers\RolesController 		AS Roles;
use App\Http\Controllers\Auth\LoginController	AS Login;
use App\Http\Controllers\UserController 		AS Usuarios;
use App\Http\Controllers\PersonasController 	AS Personas;
use App\Http\Controllers\ReferidoController 	AS Referidos;
use App\Http\Controllers\PreguntasController 	AS Preguntas;
use App\Http\Controllers\BitacorasController 	AS Bitacoras;
use App\Http\Controllers\HomeController			AS Home;






/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('login/{provider}', [Login::class, 'redirectToProvider'])->name('social.auth');
Route::get('login/{provider}/callback', [Login::class, 'handleProviderCallback'])->name('social.callback');

Auth::routes();

Route::get('/', function () {
	if(Auth::guest()){
    	return view('auth.login');
	}else{
        return redirect()->route('home')->with('status_success', 'Se ha iniciado sesion recientemente');
	}
});

Route::get('/home', [Home::class, 'index'])->name('home');


/* Perfil */
Route::get('/perfil', [Personas::class, 'perfil'])->name('perfil');
Route::get('/perfil/usuario', [Personas::class, 'perfilUnico'])->name('perfin.usuario');
Route::post('/perfil/store', [Personas::class, 'perfilStore'])->name('perfil.store');



/* Modulo de empresas */
Route::resource('/empresas', Empresas::class)->names('empresas');
Route::get('/empresas/ciudad/{codigo}', [Empresas::class, 'ciudad']);

/* Modulo de permisos */
Route::resource('/permisos', Permisos::class)->names('permisos');

/* Modulo de Roles */
Route::resource('/roles', Roles::class)->names('roles');
Route::get('/asignacion', [Roles::class, 'asignacion'])->name('asignacion');

/* Modulo de Modulos */
Route::resource('/modulos', Modulos::class)->names('modulos');

/*Modulo de Usuarios*/
Route::resource('/usuarios', Usuarios::class)->names('users');

/*Modulo de asignacion de permisos*/
Route::get('/asignacion_permisos/update/{id?}', [Permisos::class, 'edit'])->name('asignacion.edit');
Route::post('/asignacion_permisos/update/permiso/{id?}', [Permisos::class, 'update'])->name('asignacion.update');

/*Modulo de Personas*/
Route::resource('/personas', Personas::class)->names('personas');

/*Modulo de Referidos*/
Route::resource('/referidos', Referidos::class)->names('referidos');

/*Modulo de Tipo Clientes*/
Route::get('/tipo_clientes/{id}', [Referidos::class, 'tipo_clientes']);

/*Modulo de Preguntas*/
Route::resource('/preguntas', Preguntas::class)->names('preguntas');

/*Modulo de bitacoras */
Route::resource('/bitacoras', Bitacoras::class)->names('bitacoras');
Route::get('/generar/{cedula}', [Bitacoras::class, 'generar'])->name('generar');
Route::get('/generar/acciones/{valor}/{bitacora}', [Bitacoras::class, 'acciones'])->name('acciones');
Route::post('/registro/acciones', [Bitacoras::class, 'registro']);
Route::get('/informacion/referido/{cedula}', [Bitacoras::class, 'informacion_referido']);



