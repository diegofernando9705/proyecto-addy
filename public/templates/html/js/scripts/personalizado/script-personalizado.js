$(document).ready(function () {
    
    /* validacion numero de celular */

    var input = document.querySelector("#celular_movil"),
            errorMsg = document.querySelector("#error-msg"),
            validMsg = document.querySelector("#valid-msg");

    // here, the index maps to the error code returned from getValidationError - see readme
    var errorMap = ["Número inválido", "Código de país incorrecto", "Muy corto", "Demasiado largo", "Número incorrecto"];

    // initialise plugin
    var iti = window.intlTelInput(input, {
        autoHideDialCode: false,
        nationalMode: false,
        preferredCountries: ['co'],
    });

    var reset = function () {
        input.classList.remove("error");
        errorMsg.innerHTML = "";
        errorMsg.classList.add("hide");
        validMsg.classList.add("hide");
    };

    // on blur: validate
    input.addEventListener('blur', function () {
        reset();
        if (input.value.trim()) {
            if (iti.isValidNumber()) {
                validMsg.classList.remove("hide");
            } else {
                input.classList.add("error");
                var errorCode = iti.getValidationError();
                errorMsg.innerHTML = errorMap[errorCode];
                errorMsg.classList.remove("hide");
            }
        }
    });

    // on keyup / change flag: reset
    input.addEventListener('change', reset);
    input.addEventListener('keyup', reset);
    /* fin validacion numeros de celular */


    /* Validacion codigo de verificacion */

    $(document).on("keyup", "#nro_identificacion", function () {

        console.log("Tcleo");


        // Verificar que haya un numero
        let nit = document.getElementById("nro_identificacion").value;
        let isNitValid = nit >>> 0 === parseFloat(nit) ? true : false; // Validate a positive integer

        // Si es un número se calcula el Dígito de Verificación
        if (isNitValid) {
            let inputDigVerificacion = document.getElementById("digito_verificacion");
            inputDigVerificacion.value = calcularDigitoVerificacion(nit);
        }
    });

    // Calcular
    function calcularDigitoVerificacion(myNit) {

        var vpri,
                x,
                y,
                z;

        // Se limpia el Nit
        myNit = myNit.replace(/\s/g, ""); // Espacios
        myNit = myNit.replace(/,/g, ""); // Comas
        myNit = myNit.replace(/\./g, ""); // Puntos
        myNit = myNit.replace(/-/g, ""); // Guiones

        // Se valida el nit
        if (isNaN(myNit)) {

            return "";
        }
        ;

        // Procedimiento
        vpri = new Array(16);
        z = myNit.length;

        vpri[1] = 3;
        vpri[2] = 7;
        vpri[3] = 13;
        vpri[4] = 17;
        vpri[5] = 19;
        vpri[6] = 23;
        vpri[7] = 29;
        vpri[8] = 37;
        vpri[9] = 41;
        vpri[10] = 43;
        vpri[11] = 47;
        vpri[12] = 53;
        vpri[13] = 59;
        vpri[14] = 67;
        vpri[15] = 71;

        x = 0;
        y = 0;
        for (var i = 0; i < z; i++) {
            y = (myNit.substr(i, 1));
            // console.log ( y + "x" + vpri[z-i] + ":" ) ;

            x += (y * vpri [z - i]);
            // console.log ( x ) ;    
        }

        y = x % 11;
        // console.log ( y ) ;

        return (y > 1) ? 11 - y : y;
    }

    /* fin validacion codigo */


    /* Verificar Corro Empresa */
    $(document).on("keyup", "#email", function () {
        var campo = $(this).val();
        emailRegex = /^[-\w.%+]{1,64}@(?:[A-Z0-9-]{1,63}\.){1,125}[A-Z]{2,63}$/i;
        //Se muestra un texto a modo de ejemplo, luego va a ser un icono
        if (emailRegex.test(campo)) {
            $(this).attr("style", "");
            $("#error-email").attr("style", "display:none;");
            return true;
        } else {
            $(this).attr("style", "border:1px solid #FFA9A9;");
            $("#error-email").attr("style", "display:block; margin-top:5px;");
            return false;
        }
    });


    /* Verificar numero de celular */

    /*  verificar ciudad */

    $(document).on("change", "#selector", function () {
        
        $.ajax({
            type: 'GET',
            url: '/empresas/ciudad/' + $(this).val(),
            success: function (data) {
                $("#selector_ciudad").html(data);
            }
        });
    });

    /* fin verificar ciudad */

    /* registro de empresas */
    $(document).on("click", "#registro_empresa", function () {

        var formulario = $('#form_empresa').serialize();

        $.ajax({
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            method: 'POST',
            url: $("#form_empresa").attr('data-action'),
            data: formulario,
            success: function (data) {
                console.log(data);
            }
        });
    });
    /* fin registro de empresas */


    


        /* registro de empresas */
    $(document).on("click", ".asignacion", function () {

        console.log("HOLA");
        var formulario = $('#formulario_edicion').serialize();
        $.ajax({
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            url: $("#formulario_edicion").attr('data-action'),
            method: 'PUT',
            data: formulario,
            success: function (data) {
                console.log(data);
            }
        });
    });
    /* fin registro de empresas */



    /* INICO ABRIR MODALES DE INFORMACION */
    
    $(document).on("click", ".informacion", function () {
        console.log('Click');
    });

    /* FIN ABRIR MODALES DE INFORMACION */

});