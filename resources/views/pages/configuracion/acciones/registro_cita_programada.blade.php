<?php date_default_timezone_set('America/Bogota'); ?>


<form action="{{ url('/registro/acciones') }}" method="POST">
	@csrf
	@method('POST')
	<br>
	<input type="hidden" name="accion_realizada" value="{{ $valor }}">
	<input type="hidden" name="informacion_bitacora" value="{{ $informacion_bitacora[0]->codigo_bitacora }}">
	
	Realizar acción de: <b>Registrar cita programada</b> la fecha seleccionada es:
	<br><br>
	Dia y Hora de la cita <input type="datetime-local" class="form-control" name="fecha_inicio">
	<br>
	Resultado de la agenda de cita fue
	<textarea class="form-control" name="resultado_agenda"></textarea>
	<br>
	<div class="row">
		<div class="col">
			<b>Usuario que hará el registro</b>
			<input type="hidden" name="usuario_registro" value="{{ auth()->user()->id }}">
			<input type="text" class="form-control" value="{{ auth()->user()->name }}" readonly="">
		</div>
		<div class="col">
			<b>Fecha y hora del registro</b>
			<input type="text" class="form-control" name="fecha_registro" value="{{ date('Y-m-d H:i') }}" readonly="">
		</div>
	</div>
	<br><br>
	<center>
		<button type="submit" class="btn btn-success">Registrar acción realizada</button>
	</center>
</form>