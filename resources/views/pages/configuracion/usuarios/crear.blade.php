
@extends('layouts.app')

@section('title', 'Registro Modulo - Aplicativo Addy')

@section('content')
<div class="content-wrapper">
    <div class="content-header row">
    </div>
    <div class="content-body">
        <section id="dashboard-ecommerce">

            @include('pages.configuracion.mensajes.modales')

            <div class="row match-height">
                <div class="col-12 col-sm-12 col-xl-12 col-lg-12">
                    <div class="card card-congratulation-medal">
                        <div class="card-body">
                            <h3>Registro de un Usuario</h3>
                            <p class="card-text font-small-3">
                                Bienvenido, desde esta sección puedes crear una nuevo Módulo en Addy</p>
                            <!--<h3 class="mb-75 mt-2 pt-50">
                                    <a href="javascript:void(0);">$48.9k</a>
                            </h3>-->
                            <hr>
                            @include('pages.configuracion.mensajes.alertas')
                            <form id="form_roles" action="{{ route('users.store') }}" enctype="multipart/form-data" method="POST">

                                @csrf
                                @method('POST')

                                <div class="form-group">
                                    <div class="row">
                                    	<div class="col">
                                            <label><b>(*) </b>Avatar del usuario:</label>
                                            <input type="file" class="form-control" name="avatar" value="{{ old('avatar') }}" required="">
                                        </div>
                                        <div class="col">
                                            <label><b>(*) </b>Nombre del usuario:</label>
                                            <input type="text" class="form-control" name="nombre_usuario" value="{{ old('nombre_usuario') }}" required="">
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="row">
                                        <div class="col">
                                            <label><b>(*) </b>Correo del usuario:</label>
                                            <input type="email" class="form-control" name="correo_usuario" value="{{ old('correo_usuario') }}" required="">
                                        </div>
                                        <div class="col">
                                            <label><b>(*) </b>Repetir correo:</label>

                                            <div class="input-group input-group-merge">
                                                <input type="email" id="fname-icon" class="form-control" name="correo_repeat">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="row">
                                        <div class="col">
                                            <label><b>(*) </b>Contraseña del usuario:</label>
                                            <div class="input-group input-group-merge form-password-toggle mb-2">
		                                        <input type="password" class="form-control" id="basic-default-password1" placeholder="Your Password" aria-describedby="basic-default-password1" name="password" required="" />
		                                        <div class="input-group-append">
		                                            <span class="input-group-text cursor-pointer"><i data-feather="eye"></i></span>
		                                        </div>
		                                    </div>
                                        </div>
                                        <div class="col">
                                            <label><b>(*) </b>Repetir Contraseña:</label>
                                            <div class="input-group input-group-merge form-password-toggle mb-2">
		                                        <input type="password" class="form-control" id="password-confirm" placeholder="Your Password" aria-describedby="basic-default-password1" name="password_confirmation" />
		                                        <div class="input-group-append">
		                                            <span class="input-group-text cursor-pointer"><i data-feather="eye"></i></span>
		                                        </div>
		                                    </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                	<div class="row">
                                        <div class="col">
                                           <label><b>(*) </b>Rol para asignar:</label>
                                            <select class="form-control" name="rol" id="selector_rol" required="">
                                            	<option value="" selected=""></option>
                                            	@foreach($roles as $rol)
                                            		<option value="{{ $rol->id }}">{{ $rol->nombre }}</option>
                                            	@endforeach
                                            </select>
                                        </div>
                                        <div class="col">
                                            <label><b>(*) </b>Asignar a la empresa:</label>
                                            <select class="form-control" name="empresa" id="selector_empresa" required="">
                                            	<option value="" selected=""></option>
                                            	@foreach($empresas as $empresa)
                                            	<option value="{{ $empresa->id }}">{{ $empresa->indentificacion_tributaria }} - {{ $empresa->nombre_empresa }}</option>
                                            	@endforeach
                                            </select>  
                                        </div>
                                    </div>
                                </div>


                                <div class="form-group">
                                	<label><b>(*) </b>Estado del usuario:</label>
                                	<select class="form-control" name="estado_usuario" value="{{ old('estado_rol') }}" required="">
                                		<option value="">Seleccione un estado</option>
                                		<option value="1">Activo</option>
                                		<option value="2">Inactivo</option>
                                	</select>
                                </div>

                                <center>
                                    <button type="submit" class="btn btn-primary" id="registro_usuario">Registrar usuario</button>
                                </center>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>

@endsection