@extends('layouts.app')

@section('title', 'Módulo Permisos (Usuarios) - Addy')

@section('content')
<div class="content-wrapper">
    <div class="content-header row">
    </div>
    <div class="content-body">
        <section id="dashboard-ecommerce">
            <div class="row match-height">
                <div class="col-12 col-sm-12 col-xl-12 col-lg-12">
                    <div class="card card-congratulation-medal">
                        <div class="card-body">

                            <h3>M&oacute;dulo de Usuarios</h3>
                            <p class="card-text font-small-3">
                                Hola! acá verás un listado con todos los usuarios de las empresas registradas.</p>
                            <!--<h3 class="mb-75 mt-2 pt-50">
                                    <a href="javascript:void(0);">$48.9k</a>
                            </h3>-->
                            <hr>
                            <div class="panel-busqueda">
                                <div class="row">
                                    <div class="form-group col-12 col-md-9 col-lg-9 col-sm-9 col-xl-9 col-lg-9">
                                        <label><b>Búsqueda de empresas:</b></label>
                                        <input type="text" class="form-control" name="busqueda_empresa" id="busqueda" data-modulo="empresas">
                                    </div>
                                    <div class="form-group col-12 col-md-3 col-lg-3 col-sm-3 col-xl-3 col-lg-3">
                                        <label></label>
                                        <button class="btn btn-primary btn-block waves-effect waves-float waves-light" tabindex="4">Búsqueda avanzada</button>
                                    </div>
                                </div>
                                <table class="table table-bordered table-hover table-responsive ">
                                    <thead>
                                        <tr>
                                            <th>Icono o Imagen del Usuario</th>
                                            <th>Nombre del usuario</th>
                                            <th>Correo del usuario</th>
                                            <th>Empresa</th>
                                            <th colspan="4" style="text-align: center;">Acciones</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($usuarios as $usuario)
                                            @if($usuario->estado_user == 1)
                                                <tr>
                                                    <td><img src=" {{ $usuario->avatar }}" width="100px" /></td>
                                                    <td>{{ $usuario->name }}</td>
                                                    <td>{{ $usuario->email }}</td>
                                                    <td>{{ $usuario->nombre_empresa }}</td>

                                                    <td>
                                                        <button type="button" class="btn btn-success informacion" data-href="{{ route('users.show', $usuario->id) }}"> 
                                                            Ver
                                                        </button>
                                                    </td>

                                                    <td>
                                                        <button type="button" class="btn btn-info edicion" data-href="{{ route('users.edit', $usuario->id) }}"> 
                                                            Editar
                                                        </button>
                                                    </td>

                                                    <td>
                                                        <button type="button" class="btn btn-danger eliminar" data-href="{{ route('users.destroy', $usuario->id) }}"> 
                                                            Eliminar
                                                        </button>
                                                    </td>
                                                </tr>
                                            @elseif ($usuario->estado_user == 3)
                                            <tr class="alert alert-warning">
                                                    <td><img src=" {{ $usuario->avatar }}" width="100px" /></td>
                                                    <td>{{ $usuario->name }}</td>
                                                    <td>{{ $usuario->email }}</td>
                                                    <td>{{ $usuario->nombre_empresa }}</td>
                                                    <td>
                                                        <button type="button" class="btn btn-success informacion" data-href="{{ route('users.show', $usuario->id) }}"> 
                                                            Ver
                                                        </button>
                                                    </td>
                                                    <td>
                                                        <button type="button" class="btn btn-info editar" data-href="{{ route('users.edit', $usuario->id) }}"> 
                                                            Editar
                                                        </button>
                                                    </td>

                                                    <td>
                                                        <button type="button" class="btn btn-danger eliminar" data-href="{{ route('users.destroy', $usuario->id) }}"> 
                                                            Eliminar
                                                        </button>
                                                    </td>
                                                </tr>
                                            @else
                                                <tr class="alert alert-danger">
                                                    <td><img src=" {{ $usuario->avatar }}" width="100px" /></td>
                                                    <td>{{ $usuario->name }}</td>
                                                    <td>{{ $usuario->email }}</td>
                                                    <td>{{ $usuario->nombre_empresa }}</td>
                                                    <td>
                                                        <button type="button" class="btn btn-success informacion" data-href="{{ route('users.show', $usuario->id) }}"> 
                                                            Ver
                                                        </button>
                                                    </td>
                                                    <td>
                                                        <button type="button" class="btn btn-info editar" data-href="{{ route('users.edit', $usuario->id) }}"> 
                                                            Editar
                                                        </button>
                                                    </td>

                                                    <td>
                                                        <button type="button" class="btn btn-danger eliminar" data-href="{{ route('users.destroy', $usuario->id) }}"> 
                                                            Eliminar
                                                        </button>
                                                    </td>
                                                </tr>
                                            @endif
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
        </section>
    </div>
</div>

@include('pages.configuracion.mensajes.modales')

@endsection