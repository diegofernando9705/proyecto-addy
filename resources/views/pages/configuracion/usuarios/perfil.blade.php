@extends('layouts.app')

@section('title', 'Registro Personas - Aplicativo Addy')

@section('content')
<div class="content-wrapper">
    <div class="content-header row">
    </div>
    <div class="content-body">
        <section id="dashboard-ecommerce">
            @include('pages.configuracion.mensajes.alertas')
            <div class="row match-height">
                <div class="col-12 col-sm-12 col-xl-12 col-lg-12">
                    <div class="card card-congratulation-medal">
                        <div class="card-body">
                            <h3>Registro de una persona</h3>
                            <p class="card-text font-small-3">Bienvenido, desde esta sección puedes registrar una persona</p>
                            <!--<h3 class="mb-75 mt-2 pt-50">
                                    <a href="javascript:void(0);">$48.9k</a>
                            </h3>-->
                            <hr>

                            <form id="form_empresa" action="{{ route('perfil.store') }}" method="POST" enctype="multipart/form-data">

                                @csrf
                                @method('POST')

                                <div class="form-group">
                                    <img src="{{ auth()->user()->avatar }}" width="150">
                                    <label><b>(*) Foto:</b></label>
                                    <input type="file" class="form-control" name="foto_persona">
                                </div>

                                <div class="form-group">
                                    <div class="row">
                                        <div class="col">
                                            <label><b>(*) Tipo de Identificación:</b></label>
                                            <select class="form-control"  name="code_tipo_documento"  >
                                                <option value=""></option>
                                                <option value="1">CC</option>
                                                <option value="2">NIT</option>
                                                <option value="3">RUT</option>
                                            </select>
                                        </div>
                                        <div class="col">
                                            <label><b>(*) Número de Identificación:</b></label>
                                            <input type="number" class="form-control" name="cedula_persona" value="{{ old('cedula_persona') }}" >
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="row">
                                        <div class="col">
                                             <label><b>(*) Primer nombre:</b></label>
                                            <input type="text" class="form-control" name="primer_nombre" value="{{ old('primer_nombre') }}" >
                                        </div>
                                        <div class="col">
                                            <label><b>Segundo nombre:</b></label>
                                            <input type="text" class="form-control" name="segundo_nombre" value="{{ old('segundo_nombre') }}">
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="row">
                                        <div class="col">
                                             <label><b>(*) Primer Apellido:</b></label>
                                            <input type="text" class="form-control" name="primer_apellido" value="{{ old('primer_apellido') }}" >
                                        </div>
                                        <div class="col">
                                            <label><b>Segundo Apellido:</b></label>
                                            <input type="text" class="form-control" name="segundo_apellido" value="{{ old('segundo_apellido') }}">
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label><b> (*) Fecha Nacimiento:</b></label>
                                    <input type="date" class="form-control" name="fecha_nacimiento" value="{{ old('fecha_nacimiento') }}">
                                </div>

                                <div class="form-group">
                                    <div class="row">
                                        <div class="col">
                                            <label><b>(*) Email:</b></label>
                                            <input type="email" class="form-control" name="email" id="email"  value="{{ auth()->user()->email }}" readonly="">
                                            <code id="error-email" style="display: none;">Debes de digitar un correo válido.</code>
                                        </div>
                                        <div class="col">
                                            <label><b>Telefono fijo:</b></label><br>
                                            <input class='form-control' type="tel" name="telefono_fijo" value="{{ old('celular_movil') }}" >
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="row">
                                        <div class="col">
                                            <label><b>(*) Celular:</b></label>
                                            <input type="number" class="form-control" name="celular" value="{{ old('celular') }}">
                                        </div>
                                        <div class="col">
                                            <label><b>(*) WhatsApp:</b></label><br>
                                            <input class='form-control' type="tel" name="whatsApp" id="celular_movil" value="{{ old('whatsApp') }}">
                                            <span id="valid-msg" class="hide">✓ Válido</span>
                                            <span id="error-msg" class="hide"></span>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label><b> C&oacute;digo postal:</b></label>
                                    <input type="number" class="form-control" name="codigo_postal">
                                </div>

                                <div class="form-group">
                                    <div class="row">
                                        <div class="col">
                                            <label><b>Direcci&oacute;n</label>
                                            <input type="text" class="form-control" name="direccion"value="{{ old('direccion') }}" >
                                        </div>
                                        <div class="col">
                                            <label><b> (*) Empresa: </b></label>
                                            <select class="form-control" name="empresa" id="selector_permisos" readonly="">
                                                @foreach($empresas as $empresa)
                                                    @if($empresa->id == auth()->user()->id_empresa)
                                                        <option value="{{ $empresa->id }}" selected="">{{ $empresa->nombre_empresa }}</option>
                                                    @endif
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="row">
                                        <div class="col">
                                            <label>(*) País:</label>
                                            <select class="form-control" id="selector"  name="codigo_pais" value="{{ old('codigo_pais') }}">
                                                <option value="" selected=""></option>
                                                @foreach($paises as $pais)
                                                <option value="{{ $pais->codigo }}">{{ $pais->pais }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col">
                                            <label>(*) Ciudad:</label>
                                            <select class="form-control" id="selector_ciudad"  name="codigo_ciudad" value="{{ old('codigo_ciudad') }}" >
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col">
                                            <label>(*) Asignar perfil:</label>
                                            <select class="form-control"  name="perfil" value="{{ old('usuario') }}" readonly="">
                                                <option value="1" selected="">Agente</option>
                                            </select>
                                        </div>
                                        <div class="col">   
                                            <label>(*) Asignar usuario:</label>
                                            <select class="form-control"  name="usuario" value="{{ old('usuario') }}" id="selector_usuario" readonly="">
                                                @foreach($usuarios as $usuario)
                                                    @if($usuario->id == auth()->user()->id)
                                                        <option value="{{ $usuario->id }}" selected="">{{ $usuario->name }}</option>
                                                    @endif
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label><b> (*) Estado: </b></label>
                                    <select class="form-control" name="estado" >
                                        <option value="1" selected="">Activo</option>
                                    </select>
                                </div>

                                <center>
                                    <button type="submit" class="btn btn-primary" id="registro_empresa">Actualizar Perfil</button>
                                </center>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>
@endsection
