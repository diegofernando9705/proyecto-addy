
@extends('layouts.app')

@section('title', 'Registro Modulo - Aplicativo Addy')

@section('content')
<div class="content-wrapper">
    <div class="content-header row">
    </div>
    <div class="content-body">
        <section id="dashboard-ecommerce">

            @include('pages.configuracion.mensajes.modales')

            <div class="row match-height">
                <div class="col-12 col-sm-12 col-xl-12 col-lg-12">
                    <div class="card card-congratulation-medal">
                        <div class="card-body">
                            <h3>Registro de un Rol</h3>
                            <p class="card-text font-small-3">
                                Bienvenido, desde esta sección puedes crear una nuevo Módulo en Addy</p>
                            <!--<h3 class="mb-75 mt-2 pt-50">
                                    <a href="javascript:void(0);">$48.9k</a>
                            </h3>-->
                            <hr>
                            @include('pages.configuracion.mensajes.alertas')
                            <form id="form_roles" data-action="{{ route('roles.store') }}" method="POST">

                                @csrf
                                @method('POST')


                                <div class="form-group">
                                    <div class="row">
                                        <div class="col">
                                            <label><b>(*) </b>Nombre del rol:</label>
                                            <input type="text" class="form-control" name="nombre_rol" value="{{ old('nombre_rol') }}" >
                                        </div>
                                        <div class="col">
                                            <label><b>(*) </b>Slug del rol:</label>

                                            <div class="input-group input-group-merge">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text">{{ $empresa_unica[0]->indentificacion_tributaria }}</span>
                                                </div>
                                                <input type="text" id="fname-icon" class="form-control" name="slug_rol" placeholder="First Name" autocomplete="off" style="background-image: url(&quot;data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAASCAYAAABSO15qAAAAAXNSR0IArs4c6QAAAPhJREFUOBHlU70KgzAQPlMhEvoQTg6OPoOjT+JWOnRqkUKHgqWP4OQbOPokTk6OTkVULNSLVc62oJmbIdzd95NcuGjX2/3YVI/Ts+t0WLE2ut5xsQ0O+90F6UxFjAI8qNcEGONia08e6MNONYwCS7EQAizLmtGUDEzTBNd1fxsYhjEBnHPQNG3KKTYV34F8ec/zwHEciOMYyrIE3/ehKAqIoggo9inGXKmFXwbyBkmSQJqmUNe15IRhCG3byphitm1/eUzDM4qR0TTNjEixGdAnSi3keS5vSk2UDKqqgizLqB4YzvassiKhGtZ/jDMtLOnHz7TE+yf8BaDZXA509yeBAAAAAElFTkSuQmCC&quot;); background-repeat: no-repeat; background-attachment: scroll; background-size: 16px 18px; background-position: 98% 50%; cursor: auto;">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label><b>(*) </b>Descripción del rol:</label>
                                    <textarea class="form-control" name="descripcion_rol">{{ old('descripcion_modulo') }}</textarea>                                    
                                </div>


                                <div class="form-group">
                                    <div class="row">
                                        <div class="col">
                                            <label>Asignar a la empresa:</label>
                                            <select class="form-control" name="empresas[]" id="selector_empresa" multiple="multiple">
                                                @foreach($empresas as $empresa)
                                                <option value="{{ $empresa->id }}">{{ $empresa->indentificacion_tributaria }} - {{ $empresa->nombre_empresa }}</option>
                                                @endforeach
                                            </select>  
                                        </div>
                                        <div class="col">
                                            <label><b>(*) </b>Estado de lal Módulo:</label>
                                            <select class="form-control" name="estado_rol" value="{{ old('estado_rol') }}">
                                                <option value="">Seleccione un estado</option>
                                                <option value="1">Activo</option>
                                                <option value="2">Inactivo</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label><b></b>¿Acceso total?</label>
                                    <select class="form-control" name="acceso_total">
                                        <option value="" selected=""></option>
                                        <option value="yes">Si, acceso total.</option>
                                        <option value="no" selected="">No, acceso personalizado.</option>                       
                                    </select>
                                </div>

                                <hr>
                                <div class="collapse-info">
                                    <h2><b>M&oacute;dulos</b></h2>
                                    <div class="row">
                                        @foreach($permisos as $permiso)

                                        <div class="abrir col-4 col-sm-2 col-md-2 col-lg-2 col-xl-2 {{ $permiso->name_module }}">
                                            <button type="button" class="btn btn-warning collapse-header" data-toggle="collapse" role="button" data-target="#collapse{{ $permiso->id }}" aria-expanded="false" aria-controls="collapse2">
                                                M&oacute;dulo {{ $permiso->name }}
                                            </button>

                                            <div id="collapse{{ $permiso->id }}" role="tabpanel" aria-labelledby="headingCollapse2" class="collapse" aria-expanded="false">
                                                <div class="card-body">
                                                    <div class="custom-control custom-checkbox">
                                                        <input type="checkbox" name="permission[]" class="custom-control-input custom-control-success" value="{{ $permiso->id }}" id="{{ $permiso->id }}">
                                                        <label class="custom-control-label" for="{{ $permiso->id }}">
                                                            {{ $permiso->name }}
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        @endforeach 
                                    </div>
                                </div>
                                <center>
                                    <button type="button" class="btn btn-primary" id="registro_rol">Registrar Rol</button>
                                </center>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>

@endsection