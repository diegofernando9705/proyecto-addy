@extends('layouts.app')

@section('title', 'Módulo Empresas - Addy')

@section('content')
<div class="content-wrapper">
    <div class="content-header row">
    </div>
    <div class="content-body">
        <section id="dashboard-ecommerce">
            <div class="row match-height">
                <div class="col-12 col-sm-12 col-xl-12 col-lg-12">
                    <div class="card card-congratulation-medal">
                        <div class="card-body">
                            <h3>Empresas en el aplicativo Addy</h3>
                            <p class="card-text font-small-3">Hola! acá verás un listado con todas las empresas registradas.</p>
                            <!--<h3 class="mb-75 mt-2 pt-50">
                                    <a href="javascript:void(0);">$48.9k</a>
                            </h3>-->
                            <hr>
                            <div class="panel-busqueda">
                                <div class="row">
                                    <div class="form-group col-12 col-md-9 col-lg-9 col-sm-9 col-xl-9 col-lg-9">
                                        <label><b>Búsqueda de empresas:</b></label>
                                        <input type="text" class="form-control" name="busqueda_empresa" id="busqueda" data-modulo="empresas">
                                    </div>
                                    <div class="form-group col-12 col-md-3 col-lg-3 col-sm-3 col-xl-3 col-lg-3">
                                        <label></label>
                                        <button class="btn btn-primary btn-block waves-effect waves-float waves-light" tabindex="4">Búsqueda avanzada</button>
                                    </div>
                                </div>
                                <table class="table table-bordered table-hover table-responsive ">
                                    <thead>
                                        <tr>
                                            <th>NIT / CC</th>
                                            <th>Logo</th>
                                            <th>Razón Social</th>
                                            <th>Celular de contacto</th>
                                            <th colspan="4" style="text-align: center;">Acciones</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($empresas as $empresa)
                                        <tr>
                                            <td>{{ $empresa->indentificacion_tributaria }}</td>
                                            <td><img src="{{ asset('/storage/app/'.$empresa->logo_empresa) }}" width="100px"></td>
                                            <td>{{ $empresa->nombre_empresa}}</td>
                                            <td>{{ Crypt::decryptString($empresa->telefono_whatsapp) }}</td>
                                            <td>
                                                <button type="button" class="btn btn-success informacion" data-href="{{ route('empresas.show', $empresa->id) }}"> 
                                                    Ver
                                                </button>
                                            </td>
                                            <td>
                                                <a href="{{ route('empresas.edit', $empresa->id) }}">
                                                    <button type="button" class="btn btn-info"> 
                                                        Editar 
                                                    </button>
                                                </a>
                                            </td>

                                            <td>
                                                <button type="button" class="btn btn-danger eliminar" data-href="{{ route('empresas.destroy', $empresa->id) }}"> 
                                                    Eliminar
                                                </button>
                                            </td>

                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
        </section>
    </div>
</div>

@include('pages.configuracion.mensajes.modales')

@endsection