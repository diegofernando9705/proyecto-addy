@extends('layouts.app')

@section('title', 'Registro Referido - Aplicativo Addy')

@section('content')

<div class="content-wrapper">
    <div class="content-header row">
    </div>
    <div class="content-body">
        <section id="dashboard-ecommerce">
            @include('pages.configuracion.mensajes.alertas')
            <div class="row match-height">

                <!-- Profile Card -->
                    <div class="col-lg-6 col-md-6 col-12">
                        <div class="card card-profile">
                            <img src="{{ asset('templates/html/images/banner/banner-12.jpg') }}" class="img-fluid card-img-top" alt="Profile Cover Photo" />
                            <div class="card-body">
                                <div class="profile-image-wrapper">
                                    <div class="profile-image">
                                        <div class="avatar">
                                            <img src="{{ asset('templates/html/images/portrait/small/avatar-s-9.jpg') }}" alt="Profile Picture" />
                                        </div>
                                    </div>
                                </div>
                                <h3>Cliente Comprador</h3>
                                <!--
                                <h6 class="text-muted">Malaysia</h6>-->
                                <div class="badge badge-light-primary profile-badge">
                                    <a href="{{ url('tipo_clientes/cliente_comprador') }}">
                                        <button class="btn btn-success">Seleccionar</button>
                                    </a>
                                </div>
                                <hr class="mb-2" />
                                <div class="d-flex justify-content-between align-items-center">
                                    <div>
                                        <h6 class="text-muted font-weight-bolder">Registros</h6>
                                        <h3 class="mb-0">X Registros</h3>
                                    </div>
                                    <div>
                                        <h6 class="text-muted font-weight-bolder">Proyectos</h6>
                                        <h3 class="mb-0">X Registros</h3>
                                    </div>
                                    <div>
                                        <h6 class="text-muted font-weight-bolder">Nivel</h6>
                                        <h3 class="mb-0">23</h3>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-12">
                        <div class="card card-profile">
                            <img src="{{ asset('templates/html/images/banner/banner-12.jpg') }}" class="img-fluid card-img-top" alt="Profile Cover Photo" />
                            <div class="card-body">
                                <div class="profile-image-wrapper">
                                    <div class="profile-image">
                                        <div class="avatar">
                                            <img src="{{ asset('templates/html/images/portrait/small/avatar-s-9.jpg') }}" alt="Profile Picture" />
                                        </div>
                                    </div>
                                </div>
                                <h3>Cliente Vendedor</h3>
                                <!--
                                <h6 class="text-muted">Malaysia</h6>-->
                                <div class="badge badge-light-primary profile-badge">
                                    <a href="{{ url('tipo_clientes/cliente_vendedor') }}">
                                        <button class="btn btn-success">Seleccionar</button>
                                    </a>
                                </div>
                                <hr class="mb-2" />
                                <div class="d-flex justify-content-between align-items-center">
                                    <div>
                                        <h6 class="text-muted font-weight-bolder">Registros</h6>
                                        <h3 class="mb-0">X Registros</h3>
                                    </div>
                                    <div>
                                        <h6 class="text-muted font-weight-bolder">Proyectos</h6>
                                        <h3 class="mb-0">X Registros</h3>
                                    </div>
                                    <div>
                                        <h6 class="text-muted font-weight-bolder">Nivel</h6>
                                        <h3 class="mb-0">23</h3>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <!--/ Profile Card -->
            </div>
        </section>
    </div>
</div>
@endsection
@section('script')
<script type="text/javascript">

    $(document).on("change", "#selector", function () {
       
        $.ajax({
            type: 'GET',
            url: '/empresas/ciudad/' + $(this).val(),
            success: function (data) {
                $("#selector_ciudad").html(data);
            }
        });
    });
</script>
@endsection
