
@extends('layouts.app')

@section('title', 'Registro Modulo - Aplicativo Addy')

@section('content')
<div class="content-wrapper">
    <div class="content-header row">
    </div>
    <div class="content-body">
        <section id="dashboard-ecommerce">
            
            <div class="row match-height">

                <div class="col-12 col-sm-12 col-xl-12 col-lg-12">
                    @include('pages.configuracion.mensajes.alertas')
                    <div class="card card-congratulation-medal">
                        <div class="card-body">
                            <h3>Registro de una Módulo</h3>
                            <p class="card-text font-small-3">
                            Bienvenido, desde esta sección puedes crear una nuevo Módulo en Addy</p>
                            <!--<h3 class="mb-75 mt-2 pt-50">
                                    <a href="javascript:void(0);">$48.9k</a>
                            </h3>-->
                            <hr>

                            <form id="form_empresa" action="{{ route('modulos.store') }}" method="POST">

                                @csrf
                                @method('POST')

                                <div class="form-group">
                                    <label><b>(*) </b>Icono del Módulo <a href="" title="¿Por qué debo colocar esto?"><i data-feather="help-circle"></i></a></label>
                                    <input class='form-control' type="text" name="icon_modulo"  value="{{ old('icon_modulo') }}">
                                </div>

                                <div class="form-group">
                                    <div class="row">
                                        <div class="col">
                                            <label><b>(*) </b>Nombre del módulo:</label>
                                            <input type="text" class="form-control" name="nombre_modulo" value="{{ old('nombre_modulo') }}" >
                                        </div>
                                        <div class="col">
                                            <label><b>(*) </b>Slug del Módulo:</label>
                                            <input type="text" class="form-control" name="slug_modulo" value="{{ old('slug_modulo') }}">
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label><b>(*) </b>Descripción del Módulo:</label>
                                    <textarea class="form-control" name="descripcion_modulo">{{ old('descripcion_modulo') }}</textarea>                                    
                                </div>

                                <div class="form-group">
                                    <label>Asignar módulo a la empresa:</label>
                                    <select class="form-control" name="empresa_modulo[]" value="{{ old('estado_empresa') }}" id="selector_empresa" multiple="multiple">
                                        @foreach($empresas as $empresa)
                                            <option value="{{ $empresa->id }}">{{ $empresa->indentificacion_tributaria }} - {{ $empresa->nombre_empresa }}</option>
                                        @endforeach
                                    </select>                                   
                                </div>

                                <div class="form-group">
                                    <div class="row">
                                        <div class="col">
                                            <label><b>(*) </b>Sección del Módulo:</label>
                                            <select class="form-control" name="seccion_modulo" value="{{ old('estado_empresa') }}">
                                                <option value="">Seleccione un estado</option>
                                                <option value="1">APPS & PAGES</option>
                                                <option value="2">USER INTERFACE</option>
                                            </select>
                                        </div>
                                        <div class="col">
                                             <label><b>(*) </b>Estado de lal Módulo:</label>
                                            <select class="form-control" name="estado_modulo" value="{{ old('estado_empresa') }}">
                                                <option value="">Seleccione un estado</option>
                                                <option value="1">Activo</option>
                                                <option value="2">Inactivo</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>


                                <div class="form-group">
                                    <label><b>(*) </b>Permisos habilitados del Módulo:</label>
                                    <select class="form-control" name="permisos[]" value="{{ old('estado_empresa') }}" id="selector_permisos" multiple="multiple">
                                        <option value="habilitado" selected="">Activar Modulo</option>
                                        <option value="crear">Crear</option>
                                        <option value="editar">Editar</option>
                                        <option value="ver">Ver</option>
                                        <option value="eliminar">Eliminar</option>
                                    </select>
                                </div>


                                <center>
                                    <button type="submit" class="btn btn-primary" id="registro_empresa">Registrar Módulo</button>
                                </center>
                            </form>


                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>
@endsection