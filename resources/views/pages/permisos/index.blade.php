@extends('layouts.app')

@section('title', 'Módulo Permisos - Addy')

@section('content')
<div class="content-wrapper">
	<div class="content-header row">
	</div>
	<div class="content-body">
		<section id="dashboard-ecommerce">
			<div class="row match-height">
				<div class="col-12 col-sm-12 col-xl-12 col-lg-12">
					<div class="card card-congratulation-medal">
						<div class="card-body">
							<h3>Módulo de Permisos</h3>
							<p class="card-text font-small-3">Bienvenido, desde esta sección puedes administrar los permisos de Addy</p>
							<!--<h3 class="mb-75 mt-2 pt-50">
								<a href="javascript:void(0);">$48.9k</a>
							</h3>-->
							<hr>
							<form>
								<div class="form-group">
									<label>Nombre permiso:</label>
									<input type="text" class="form-control" name="nombre_permiso">
								</div>

								<div class="form-group">
									<label>Slug permiso:</label>
									<input type="text" class="form-control" name="nombre_permiso">
								</div>

								<div class="form-group">
									<label>Descripción del permiso:</label>
									<input type="text" class="form-control" name="nombre_permiso">
								</div>
							</form>

							<center>
								<button type="button" class="btn btn-primary">Registrar permiso</button>
							</center>
						</div>
					</div>
				</div>
			</div>
		</section>
	</div>
</div>
@endsection