<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
    <!-- BEGIN: Head-->

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
        <meta name="description" content="Vuexy admin is super flexible, powerful, clean &amp; modern responsive bootstrap 4 admin template with unlimited possibilities.">
        <meta name="keywords" content="admin template, Vuexy admin template, dashboard template, flat admin template, responsive admin template, web app">
        <meta name="author" content="PIXINVENT">
        <title>@yield('title', 'Principal - Addy')</title>

        <link rel="apple-touch-icon" href="{{ asset('templates/html/images/ico/apple-icon-120.png') }}">
        <link rel="shortcut icon" type="image/x-icon" href="{{ asset('templates/html/images/ico/favicon.ico') }}">
        <link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,300;0,400;0,500;0,600;1,400;1,500;1,600" rel="stylesheet">

        <!-- BEGIN: Vendor CSS-->
        <link rel="stylesheet" type="text/css" href="{{ asset('templates/html/vendors/css/vendors.min.css') }}">
        <!-- END: Vendor CSS-->

        <!-- BEGIN: Theme CSS-->
        <link rel="stylesheet" type="text/css" href="{{ asset('templates/html/css/bootstrap.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ asset('templates/html/css/bootstrap-extended.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ asset('templates/html/css/colors.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ asset('templates/html/css/components.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ asset('templates/html/css/themes/dark-layout.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ asset('templates/html/css/themes/bordered-layout.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ asset('templates/html/css/themes/semi-dark-layout.css') }}">

        <!-- BEGIN: Page CSS-->
        <link rel="stylesheet" type="text/css" href="{{ asset('templates/html/css/core/menu/menu-types/vertical-menu.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ asset('templates/html/css/plugins/forms/form-validation.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ asset('templates/html/css/pages/page-auth.css') }}">
        <!-- END: Page CSS-->

        <!-- BEGIN: Custom CSS-->
        <link rel="stylesheet" type="text/css" href="{{ asset('templates/html/style.css') }}">
        <!-- END: Custom CSS-->
        <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />

        <link rel="stylesheet" type="text/css" href="{{ asset('templates/html/css/personalizado/validate-celular/intlTelInput.css') }}">
        <style type="text/css">
            .hide{
                display: none;
            }
            .iti__flag {
                background-image: url("{{ asset('templates/html/images/logo/flags.png') }}");
            }

            @media (-webkit-min-device-pixel-ratio: 2), (min-resolution: 192dpi) {
                .iti__flag {
                    background-image: url("{{ asset('templates/html/images/logo/flags@2x.png') }}");
                }
            }
        </style>
    </head>
    <!-- END: Head-->
    <body class="vertical-layout vertical-menu-modern  navbar-floating footer-static  " data-open="click" data-menu="vertical-menu-modern" data-col="">

        <!-- BEGIN: Header-->
        <nav class="header-navbar navbar navbar-expand-lg align-items-center floating-nav navbar-light navbar-shadow">
            <div class="navbar-container d-flex content">
                <div class="bookmark-wrapper d-flex align-items-center">
                    <ul class="nav navbar-nav d-xl-none">
                        <li class="nav-item"><a class="nav-link menu-toggle" href="javascript:void(0);"><i class="ficon" data-feather="menu"></i></a></li>
                    </ul>
                    <ul class="nav navbar-nav bookmark-icons">
                        <li class="nav-item d-none d-lg-block"><a class="nav-link" href="app-email.html" data-toggle="tooltip" data-placement="top" title="Email"><i class="ficon" data-feather="mail"></i></a></li>
                        <li class="nav-item d-none d-lg-block"><a class="nav-link" href="app-chat.html" data-toggle="tooltip" data-placement="top" title="Chat"><i class="ficon" data-feather="message-square"></i></a></li>
                        <li class="nav-item d-none d-lg-block"><a class="nav-link" href="app-calendar.html" data-toggle="tooltip" data-placement="top" title="Calendar"><i class="ficon" data-feather="calendar"></i></a></li>
                        <li class="nav-item d-none d-lg-block"><a class="nav-link" href="app-todo.html" data-toggle="tooltip" data-placement="top" title="Todo"><i class="ficon" data-feather="check-square"></i></a></li>
                    </ul>
                    <ul class="nav navbar-nav">
                        <li class="nav-item d-none d-lg-block"><a class="nav-link bookmark-star"><i class="ficon text-warning" data-feather="star"></i></a>
                            <div class="bookmark-input search-input">
                                <div class="bookmark-input-icon"><i data-feather="search"></i></div>
                                <input class="form-control input" type="text" placeholder="Bookmark" tabindex="0" data-search="search">
                                <ul class="search-list search-list-bookmark"></ul>
                            </div>
                        </li>
                    </ul>
                </div>
                <ul class="nav navbar-nav align-items-center ml-auto">
                    <li class="nav-item dropdown dropdown-language"><a class="nav-link dropdown-toggle" id="dropdown-flag" href="javascript:void(0);" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="flag-icon flag-icon-us"></i><span class="selected-language">English</span></a>
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdown-flag"><a class="dropdown-item" href="javascript:void(0);" data-language="en"><i class="flag-icon flag-icon-us"></i> English</a><a class="dropdown-item" href="javascript:void(0);" data-language="fr"><i class="flag-icon flag-icon-fr"></i> French</a><a class="dropdown-item" href="javascript:void(0);" data-language="de"><i class="flag-icon flag-icon-de"></i> German</a><a class="dropdown-item" href="javascript:void(0);" data-language="pt"><i class="flag-icon flag-icon-pt"></i> Portuguese</a></div>
                    </li>
                    <li class="nav-item d-none d-lg-block"><a class="nav-link nav-link-style"><i class="ficon" data-feather="moon"></i></a></li>
                    <li class="nav-item nav-search"><a class="nav-link nav-link-search"><i class="ficon" data-feather="search"></i></a>
                        <div class="search-input">
                            <div class="search-input-icon"><i data-feather="search"></i></div>
                            <input class="form-control input" type="text" placeholder="Explore Vuexy..." tabindex="-1" data-search="search">
                            <div class="search-input-close"><i data-feather="x"></i></div>
                            <ul class="search-list search-list-main"></ul>
                        </div>
                    </li>
                    <li class="nav-item dropdown dropdown-cart mr-25"><a class="nav-link" href="javascript:void(0);" data-toggle="dropdown"><i class="ficon" data-feather="shopping-cart"></i><span class="badge badge-pill badge-primary badge-up cart-item-count">6</span></a>
                        <ul class="dropdown-menu dropdown-menu-media dropdown-menu-right">
                            <li class="dropdown-menu-header">
                                <div class="dropdown-header d-flex">
                                    <h4 class="notification-title mb-0 mr-auto">My Cart</h4>
                                    <div class="badge badge-pill badge-light-primary">4 Items</div>
                                </div>
                            </li>
                            <li class="scrollable-container media-list">
                                <div class="media align-items-center"><img class="d-block rounded mr-1" src="../../../app-assets/images/pages/eCommerce/1.png" alt="donuts" width="62">
                                    <div class="media-body"><i class="ficon cart-item-remove" data-feather="x"></i>
                                        <div class="media-heading">
                                            <h6 class="cart-item-title"><a class="text-body" href="app-ecommerce-details.html"> Apple watch 5</a></h6><small class="cart-item-by">By Apple</small>
                                        </div>
                                        <div class="cart-item-qty">
                                            <div class="input-group">
                                                <input class="touchspin-cart" type="number" value="1">
                                            </div>
                                        </div>
                                        <h5 class="cart-item-price">$374.90</h5>
                                    </div>
                                </div>
                                <div class="media align-items-center"><img class="d-block rounded mr-1" src="../../../app-assets/images/pages/eCommerce/7.png" alt="donuts" width="62">
                                    <div class="media-body"><i class="ficon cart-item-remove" data-feather="x"></i>
                                        <div class="media-heading">
                                            <h6 class="cart-item-title"><a class="text-body" href="app-ecommerce-details.html"> Google Home Mini</a></h6><small class="cart-item-by">By Google</small>
                                        </div>
                                        <div class="cart-item-qty">
                                            <div class="input-group">
                                                <input class="touchspin-cart" type="number" value="3">
                                            </div>
                                        </div>
                                        <h5 class="cart-item-price">$129.40</h5>
                                    </div>
                                </div>
                                <div class="media align-items-center"><img class="d-block rounded mr-1" src="../../../app-assets/images/pages/eCommerce/2.png" alt="donuts" width="62">
                                    <div class="media-body"><i class="ficon cart-item-remove" data-feather="x"></i>
                                        <div class="media-heading">
                                            <h6 class="cart-item-title"><a class="text-body" href="app-ecommerce-details.html"> iPhone 11 Pro</a></h6><small class="cart-item-by">By Apple</small>
                                        </div>
                                        <div class="cart-item-qty">
                                            <div class="input-group">
                                                <input class="touchspin-cart" type="number" value="2">
                                            </div>
                                        </div>
                                        <h5 class="cart-item-price">$699.00</h5>
                                    </div>
                                </div>
                                <div class="media align-items-center"><img class="d-block rounded mr-1" src="../../../app-assets/images/pages/eCommerce/3.png" alt="donuts" width="62">
                                    <div class="media-body"><i class="ficon cart-item-remove" data-feather="x"></i>
                                        <div class="media-heading">
                                            <h6 class="cart-item-title"><a class="text-body" href="app-ecommerce-details.html"> iMac Pro</a></h6><small class="cart-item-by">By Apple</small>
                                        </div>
                                        <div class="cart-item-qty">
                                            <div class="input-group">
                                                <input class="touchspin-cart" type="number" value="1">
                                            </div>
                                        </div>
                                        <h5 class="cart-item-price">$4,999.00</h5>
                                    </div>
                                </div>
                                <div class="media align-items-center"><img class="d-block rounded mr-1" src="../../../app-assets/images/pages/eCommerce/5.png" alt="donuts" width="62">
                                    <div class="media-body"><i class="ficon cart-item-remove" data-feather="x"></i>
                                        <div class="media-heading">
                                            <h6 class="cart-item-title"><a class="text-body" href="app-ecommerce-details.html"> MacBook Pro</a></h6><small class="cart-item-by">By Apple</small>
                                        </div>
                                        <div class="cart-item-qty">
                                            <div class="input-group">
                                                <input class="touchspin-cart" type="number" value="1">
                                            </div>
                                        </div>
                                        <h5 class="cart-item-price">$2,999.00</h5>
                                    </div>
                                </div>
                            </li>
                            <li class="dropdown-menu-footer">
                                <div class="d-flex justify-content-between mb-1">
                                    <h6 class="font-weight-bolder mb-0">Total:</h6>
                                    <h6 class="text-primary font-weight-bolder mb-0">$10,999.00</h6>
                                </div><a class="btn btn-primary btn-block" href="app-ecommerce-checkout.html">Checkout</a>
                            </li>
                        </ul>
                    </li>
                    <li class="nav-item dropdown dropdown-notification mr-25"><a class="nav-link" href="javascript:void(0);" data-toggle="dropdown"><i class="ficon" data-feather="bell"></i><span class="badge badge-pill badge-danger badge-up">5</span></a>
                        <ul class="dropdown-menu dropdown-menu-media dropdown-menu-right">
                            <li class="dropdown-menu-header">
                                <div class="dropdown-header d-flex">
                                    <h4 class="notification-title mb-0 mr-auto">Notifications</h4>
                                    <div class="badge badge-pill badge-light-primary">6 New</div>
                                </div>
                            </li>
                            <li class="scrollable-container media-list"><a class="d-flex" href="javascript:void(0)">
                                    <div class="media d-flex align-items-start">
                                        <div class="media-left">
                                            <div class="avatar"><img src="../../../app-assets/images/portrait/small/avatar-s-15.jpg" alt="avatar" width="32" height="32"></div>
                                        </div>
                                        <div class="media-body">
                                            <p class="media-heading"><span class="font-weight-bolder">Congratulation Sam 🎉</span>winner!</p><small class="notification-text"> Won the monthly best seller badge.</small>
                                        </div>
                                    </div>
                                </a><a class="d-flex" href="javascript:void(0)">
                                    <div class="media d-flex align-items-start">
                                        <div class="media-left">
                                            <div class="avatar"><img src="../../../app-assets/images/portrait/small/avatar-s-3.jpg" alt="avatar" width="32" height="32"></div>
                                        </div>
                                        <div class="media-body">
                                            <p class="media-heading"><span class="font-weight-bolder">New message</span>&nbsp;received</p><small class="notification-text"> You have 10 unread messages</small>
                                        </div>
                                    </div>
                                </a><a class="d-flex" href="javascript:void(0)">
                                    <div class="media d-flex align-items-start">
                                        <div class="media-left">
                                            <div class="avatar bg-light-danger">
                                                <div class="avatar-content">MD</div>
                                            </div>
                                        </div>
                                        <div class="media-body">
                                            <p class="media-heading"><span class="font-weight-bolder">Revised Order 👋</span>&nbsp;checkout</p><small class="notification-text"> MD Inc. order updated</small>
                                        </div>
                                    </div>
                                </a>
                                <div class="media d-flex align-items-center">
                                    <h6 class="font-weight-bolder mr-auto mb-0">System Notifications</h6>
                                    <div class="custom-control custom-control-primary custom-switch">
                                        <input class="custom-control-input" id="systemNotification" type="checkbox" checked="">
                                        <label class="custom-control-label" for="systemNotification"></label>
                                    </div>
                                </div><a class="d-flex" href="javascript:void(0)">
                                    <div class="media d-flex align-items-start">
                                        <div class="media-left">
                                            <div class="avatar bg-light-danger">
                                                <div class="avatar-content"><i class="avatar-icon" data-feather="x"></i></div>
                                            </div>
                                        </div>
                                        <div class="media-body">
                                            <p class="media-heading"><span class="font-weight-bolder">Server down</span>&nbsp;registered</p><small class="notification-text"> USA Server is down due to hight CPU usage</small>
                                        </div>
                                    </div>
                                </a><a class="d-flex" href="javascript:void(0)">
                                    <div class="media d-flex align-items-start">
                                        <div class="media-left">
                                            <div class="avatar bg-light-success">
                                                <div class="avatar-content"><i class="avatar-icon" data-feather="check"></i></div>
                                            </div>
                                        </div>
                                        <div class="media-body">
                                            <p class="media-heading"><span class="font-weight-bolder">Sales report</span>&nbsp;generated</p><small class="notification-text"> Last month sales report generated</small>
                                        </div>
                                    </div>
                                </a><a class="d-flex" href="javascript:void(0)">
                                    <div class="media d-flex align-items-start">
                                        <div class="media-left">
                                            <div class="avatar bg-light-warning">
                                                <div class="avatar-content"><i class="avatar-icon" data-feather="alert-triangle"></i></div>
                                            </div>
                                        </div>
                                        <div class="media-body">
                                            <p class="media-heading"><span class="font-weight-bolder">High memory</span>&nbsp;usage</p><small class="notification-text"> BLR Server using high memory</small>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <li class="dropdown-menu-footer"><a class="btn btn-primary btn-block" href="javascript:void(0)">Read all notifications</a></li>
                        </ul>
                    </li>
                    <li class="nav-item dropdown dropdown-user"><a class="nav-link dropdown-toggle dropdown-user-link" id="dropdown-user" href="javascript:void(0);" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <div class="user-nav d-sm-flex d-none"><span class="user-name font-weight-bolder">{{ auth()->user()->name }}</span><span class="user-status">{{ auth()->user()->nombre }}</span></div><span class="avatar"><img class="round" src="{{ auth()->user()->avatar }} " alt="avatar" height="40" width="40"><span class="avatar-status-online"></span></span>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdown-user"><a class="dropdown-item" href="{{ url('perfil') }}"><i class="mr-50" data-feather="user"></i> Perfil</a><a class="dropdown-item" href="app-email.html"><i class="mr-50" data-feather="mail"></i> Inbox</a><a class="dropdown-item" href="app-todo.html"><i class="mr-50" data-feather="check-square"></i> Task</a><a class="dropdown-item" href="app-chat.html"><i class="mr-50" data-feather="message-square"></i> Chats</a>
                            <div class="dropdown-divider"></div><a class="dropdown-item" href="page-account-settings.html"><i class="mr-50" data-feather="settings"></i> Settings</a><a class="dropdown-item" href="page-pricing.html"><i class="mr-50" data-feather="credit-card"></i> Pricing</a><a class="dropdown-item" href="page-faq.html"><i class="mr-50" data-feather="help-circle"></i> FAQ</a>

                            <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
                                        document.getElementById('logout-form').submit();"><i class="mr-50" data-feather="power"></i>Cerrar</a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                @csrf
                            </form>
                        </div>
                    </li>
                </ul>
            </div>
        </nav>
        <ul class="main-search-list-defaultlist d-none">
            <li class="d-flex align-items-center"><a href="javascript:void(0);">
                    <h6 class="section-label mt-75 mb-0">Files</h6>
                </a></li>
            <li class="auto-suggestion"><a class="d-flex align-items-center justify-content-between w-100" href="app-file-manager.html">
                    <div class="d-flex">
                        <div class="mr-75"><img src="../../../app-assets/images/icons/xls.png" alt="png" height="32"></div>
                        <div class="search-data">
                            <p class="search-data-title mb-0">Two new item submitted</p><small class="text-muted">Marketing Manager</small>
                        </div>
                    </div><small class="search-data-size mr-50 text-muted">&apos;17kb</small>
                </a></li>
            <li class="auto-suggestion"><a class="d-flex align-items-center justify-content-between w-100" href="app-file-manager.html">
                    <div class="d-flex">
                        <div class="mr-75"><img src="../../../app-assets/images/icons/jpg.png" alt="png" height="32"></div>
                        <div class="search-data">
                            <p class="search-data-title mb-0">52 JPG file Generated</p><small class="text-muted">FontEnd Developer</small>
                        </div>
                    </div><small class="search-data-size mr-50 text-muted">&apos;11kb</small>
                </a></li>
            <li class="auto-suggestion"><a class="d-flex align-items-center justify-content-between w-100" href="app-file-manager.html">
                    <div class="d-flex">
                        <div class="mr-75"><img src="../../../app-assets/images/icons/pdf.png" alt="png" height="32"></div>
                        <div class="search-data">
                            <p class="search-data-title mb-0">25 PDF File Uploaded</p><small class="text-muted">Digital Marketing Manager</small>
                        </div>
                    </div><small class="search-data-size mr-50 text-muted">&apos;150kb</small>
                </a></li>
            <li class="auto-suggestion"><a class="d-flex align-items-center justify-content-between w-100" href="app-file-manager.html">
                    <div class="d-flex">
                        <div class="mr-75"><img src="../../../app-assets/images/icons/doc.png" alt="png" height="32"></div>
                        <div class="search-data">
                            <p class="search-data-title mb-0">Anna_Strong.doc</p><small class="text-muted">Web Designer</small>
                        </div>
                    </div><small class="search-data-size mr-50 text-muted">&apos;256kb</small>
                </a></li>
            <li class="d-flex align-items-center"><a href="javascript:void(0);">
                    <h6 class="section-label mt-75 mb-0">Members</h6>
                </a></li>
            <li class="auto-suggestion"><a class="d-flex align-items-center justify-content-between py-50 w-100" href="app-user-view.html">
                    <div class="d-flex align-items-center">
                        <div class="avatar mr-75"><img src="../../../app-assets/images/portrait/small/avatar-s-8.jpg" alt="png" height="32"></div>
                        <div class="search-data">
                            <p class="search-data-title mb-0">John Doe</p><small class="text-muted">UI designer</small>
                        </div>
                    </div>
                </a></li>
            <li class="auto-suggestion"><a class="d-flex align-items-center justify-content-between py-50 w-100" href="app-user-view.html">
                    <div class="d-flex align-items-center">
                        <div class="avatar mr-75"><img src="../../../app-assets/images/portrait/small/avatar-s-1.jpg" alt="png" height="32"></div>
                        <div class="search-data">
                            <p class="search-data-title mb-0">Michal Clark</p><small class="text-muted">FontEnd Developer</small>
                        </div>
                    </div>
                </a></li>
            <li class="auto-suggestion"><a class="d-flex align-items-center justify-content-between py-50 w-100" href="app-user-view.html">
                    <div class="d-flex align-items-center">
                        <div class="avatar mr-75"><img src="../../../app-assets/images/portrait/small/avatar-s-14.jpg" alt="png" height="32"></div>
                        <div class="search-data">
                            <p class="search-data-title mb-0">Milena Gibson</p><small class="text-muted">Digital Marketing Manager</small>
                        </div>
                    </div>
                </a></li>
            <li class="auto-suggestion"><a class="d-flex align-items-center justify-content-between py-50 w-100" href="app-user-view.html">
                    <div class="d-flex align-items-center">
                        <div class="avatar mr-75"><img src="../../../app-assets/images/portrait/small/avatar-s-6.jpg" alt="png" height="32"></div>
                        <div class="search-data">
                            <p class="search-data-title mb-0">Anna Strong</p><small class="text-muted">Web Designer</small>
                        </div>
                    </div>
                </a></li>
        </ul>
        <ul class="main-search-list-defaultlist-other-list d-none">
            <li class="auto-suggestion justify-content-between"><a class="d-flex align-items-center justify-content-between w-100 py-50">
                    <div class="d-flex justify-content-start"><span class="mr-75" data-feather="alert-circle"></span><span>No results found.</span></div>
                </a></li>
        </ul>
        <!-- END: Header-->

        <!-- BEGIN: Main Menu-->
        <div class="main-menu menu-fixed menu-light menu-accordion menu-shadow" data-scroll-to-active="true">
            <div class="navbar-header">
                <ul class="nav navbar-nav flex-row">
                    <li class="nav-item mr-auto">
                        <a class="navbar-brand" href="{{ route('home') }}">
                            <center>
                                <img src="{{ asset('templates/html/images/logo/logo_addy.png') }}" width="120px">
                            </center>
                        </a>
                    </li>
                    <li class="nav-item nav-toggle"><a class="nav-link modern-nav-toggle pr-0" data-toggle="collapse"><i class="d-block d-xl-none text-primary toggle-icon font-medium-4" data-feather="x"></i><i class="d-none d-xl-block collapse-toggle-icon font-medium-4  text-primary" data-feather="disc" data-ticon="disc"></i></a></li>
                </ul>
            </div>
            <div class="shadow-bottom"></div>
            <div class="main-menu-content">
                @include('layouts.sections.menu-vertical')
            </div>
        </div>
        <!-- END: Main Menu-->

        <div class="app-content content">
            <div class="content-overlay"></div>
            <div class="header-navbar-shadow"></div>
            @yield('content')
        </div>

        <div class="sidenav-overlay"></div>
        <div class="drag-target"></div>

        <!-- BEGIN: Footer-->
        <footer class="footer footer-static footer-light">
            <p class="clearfix mb-0"><span class="float-md-left d-block d-md-inline-block mt-25">Copyright &copy; 2020<a class="ml-25" href="https://1.envato.market/pixinvent_portfolio" target="_blank">Addy</a><span class="d-none d-sm-inline-block">, All rights Reserved</span></span><span class="float-md-right d-none d-md-block">Addy<i data-feather="heart"></i></span></p>
        </footer>
        

        <button class="btn btn-primary btn-icon scroll-top" type="button"><i data-feather="arrow-up"></i></button>
        <!-- END: Footer-->

        <!-- BEGIN: Vendor JS-->
        <script src="{{ asset('templates/html/vendors/js/vendors.min.js') }}"></script>
        <!-- BEGIN Vendor JS-->

        <!-- BEGIN: Page Vendor JS-->
        <script src="{{ asset('templates/html/vendors/js/forms/validation/jquery.validate.min.js') }}"></script>
        <!-- END: Page Vendor JS-->

        <!-- BEGIN: Theme JS-->
        <script src="{{ asset('templates/html/js/core/app-menu.js') }}"></script>
        <script src="{{ asset('templates/html/js/core/app.js') }}"></script>
        <!-- END: Theme JS-->

        <!-- BEGIN: Page JS-->
        <script src="{{ asset('templates/html/js/scripts/pages/page-auth-login.js') }}"></script>
        <!-- END: Page JS-->

        <script src="{{ asset('templates/html/js/scripts/personalizado/validate-celular/utils.js') }}"></script>
        <script src="{{ asset('templates/html/js/scripts/personalizado/validate-celular/intlTelInput.js') }}"></script>
        <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
        <script src="{{ asset('templates/html/js/scripts/personalizado/script-personalizado.js') }}"></script>
        @yield('script')
        <script>

            $(window).on('load', function () {
                var txtv = window.document.getElementsByClassName("informacion");

                if (feather) {
                    feather.replace({
                        width: 14,
                        height: 14
                    });
                }
            });

            $("#selector").select2();
            $("#selector_ciudad").select2();
            $("#selector_empresa").select2();
            $("#selector_permisos").select2();
            $("#selector_rol").select2();
            $("#selector_usuario").select2();
            $("#tipo_cliente").select2();




            $(".palabras_claves").select2({
                tags: true,
                tokenSeparators: [',', ' ']
            });

            /* consulta de perfil */
            $(document).ready(function(){
                $.ajax({
                    type: 'get',
                    url: "/perfil",
                    success: function (data) {
                        
                        if(data == 'sin_registro'){
                            $(".opcion-administrador").attr('style','display:none !important;');

                            $(".opcion_adicional").html("<center><a class='alert alert-danger' href='{{ url('perfil/usuario') }}'>MI Perfil - Incompleto</a></center>");
                        }else if(data == 'registro'){
                             $(".opcion-administrador").attr('style','display:none !important;');

                            $(".opcion_adicional").html("<center><a class='alert alert-success' href='{{ url('perfil') }}'>MI Perfil - Completo</a></center>");
                        }else{
                            
                        }
                    }
                });    
            });
             

            $(document).on("click", ".bitacoras", function(){
                    
                $("#bitacoras").modal("show");

                $.ajax({
                    type: 'get',
                    url: $(this).attr("data-href"),
                    success: function (data) {
                        $("#body-modal-bitacora").html(data);
                    }
                });
            });

            $(document).on("click", ".informacion_referido", function(){
                    
                $("#informacion_referido").modal("show");
                var cedula = $(this).attr("data-cedula");

                $.ajax({
                    type: 'get',
                    url: "/informacion/referido/"+cedula,
                    success: function (data) {
                        $("#body-modal-informacion_referido").html(data);
                    }
                });
            });

            $(document).on("change", ".accion_bitacora", function(){
                
                var valor = $(this).val();
                var bitacora = $(this).attr("data-bitacora");
                
                $("#acciones").modal("show");

                $.ajax({
                    type: 'get',
                    url: "/generar/acciones/"+valor+"/"+bitacora,
                    success: function (data) {
                        $("#body-modal-acciones").html(data);
                    }
                });
            });


            $(document).on("click", ".informacion", function(){
                
                $("#informacion").modal("show");

                $.ajax({
                    type: 'get',
                    url: $(this).attr("data-href"),
                    success: function (data) {
                        $("#body-modal").html(data);
                    }
                });
            });

            $(document).on("click", ".edicion", function(){
                
                $("#editar").modal("show");
                
                $.ajax({
                    type: 'get',
                    url: $(this).attr("data-href"),
                    success: function (data) {
                        $("#body-modal-edicion").html(data);
                    }
                });
                
            });
            
            
            $(document).on("click", ".asignacion", function(){
                
                $("#asignacion-permisos").modal("show");
                
                $.ajax({
                    type: 'get',
                    url: $(this).attr("data-href"),
                    success: function (data) {
                        $("#body-modal-asignacion").html(data);
                    }
                }); 
            });
            
            $(document).on("change", "#tipo_cliente", function(){
                
                $.ajax({
                    type: 'get',
                    url: "/tipo_clientes/"+$(this).val(),
                    success: function (data) {
                        $("#opciones_formulario").html(data);
                    }
                }); 

            });

            $(document).on("click", "#creacion", function(){
                
                var formData = $("#formulario").serialize();
                            document.getElementById('creacion').disabled = true;

                $.ajax({
                    headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                    type: "POST",
                    url: $("#formulario").attr("action"),
                    data: formData,
                    success: function (data) {
                        if(data == true){
                            alert('Registro exitoso');
                            setTimeout(function(){
                                location.reload();
                            }, 500)
                        }else{
                            document.getElementById('creacion').disabled = false;
                        }
                    },
                    error: function (err) {
                        console.log(err);
                        if (err.status == 422) {
                            alert('Algunos campos están vacíos');
                        }else{
                            document.getElementById('creacion').disabled = false;
                            alert(err.statusText);
                        }
                    }
                }); 
            });

            $(document).on("click", "#actualizar", function(){
                
                var formData = $("#formulario").serialize();
                console.log(formData);

                $.ajax({
                    headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                    type: "PUT",
                    url: $("#formulario").attr("action"),
                    data: formData,
                    success: function (data) {
                        
                        if(data == true){
                            alert('Registro exitoso');
                            setTimeout(function(){
                                location.reload();
                            }, 500)
                        }else{
                            document.getElementById('actualizar').disabled = false;
                        }
                    },
                    error: function (err) {
                        console.log(err);
                        if (err.status == 422) {
                            alert('Algunos campos están vacíos');
                        }else{
                            document.getElementById('actualizar').disabled = false;
                            alert(err.statusText);
                        }
                    }
                }); 
            });
            
            
            $(document).on("click", "#registro_rol", function(){
                document.getElementById('registro_rol').disabled = true;
                $('.span-error').attr("style", "display:none");
                
                var formData = $("#form_roles").serialize();
                console.log('Registro rol');
                $.ajax({
                    headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                    type: "POST",
                    url: $("#form_roles").attr("data-action"),
                    data: formData,
                    success: function (data) {
                        console.log(data);
                        if(data == true){
                            alert('Registro exitoso')
                            setTimeout(function(){
                                location.reload();
                            }, 1000)
                        }else{
                            document.getElementById('registro_rol').disabled = false;
                        }
                    },
                    error: function (err) {
                        if (err.status == 422) {
                            document.getElementById('registro_rol').disabled = false;
           
                            console.log(err.responseJSON);
                            $('#success_message').fadeIn().html(err.responseJSON.message);
                            
                           
                            console.warn(err.responseJSON.errors);
                            
                            $.each(err.responseJSON.errors, function (i, error) {
                                var el = $(document).find('[name="'+i+'"]');
                                el.after($('<span class="span-error" style="color: red;">'+error[0]+'</span>'));
                            });
                        }else{
                            document.getElementById('registro_rol').disabled = false;
                            alert(err.statusText);
                        }
                    }

                }); 
            });
            
            
            
            
            
        </script>
    </body>
    <!-- END: Body-->

</html>