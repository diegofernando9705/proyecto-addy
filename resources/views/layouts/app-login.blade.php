<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<!-- BEGIN: Head-->

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description" content="Vuexy admin is super flexible, powerful, clean &amp; modern responsive bootstrap 4 admin template with unlimited possibilities.">
    <meta name="keywords" content="admin template, Vuexy admin template, dashboard template, flat admin template, responsive admin template, web app">
    <meta name="author" content="PIXINVENT">
    <title>@yield('title', 'Inicio de Sesión - Addy')</title>

    <link rel="apple-touch-icon" href="{{ asset('templates/html/images/logo/logo_addy.png') }}">
    <link rel="shortcut icon" type="image/x-icon" href="{{ asset('templates/html/images/logo/logo_addy.png') }}">
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,300;0,400;0,500;0,600;1,400;1,500;1,600" rel="stylesheet">

    <!-- BEGIN: Vendor CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('templates/html/vendors/css/vendors.min.css') }}">
    <!-- END: Vendor CSS-->

    <!-- BEGIN: Theme CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('templates/html/css/bootstrap.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('templates/html/css/bootstrap-extended.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('templates/html/css/colors.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('templates/html/css/components.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('templates/html/css/themes/dark-layout.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('templates/html/css/themes/bordered-layout.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('templates/html/css/themes/semi-dark-layout.css') }}">

    <!-- BEGIN: Page CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('templates/html/css/core/menu/menu-types/vertical-menu.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('templates/html/css/plugins/forms/form-validation.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('templates/html/css/pages/page-auth.css') }}">
    <!-- END: Page CSS-->

    <!-- BEGIN: Custom CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('templates/html/style.css') }}">
    <!-- END: Custom CSS-->

</head>
<!-- END: Head-->
	<body class="vertical-layout vertical-menu-modern blank-page navbar-floating footer-static  " data-open="click" data-menu="vertical-menu-modern" data-col="blank-page">
		<div class="app-content content">
			<div class="content-overlay"></div>
        	<div class="header-navbar-shadow"></div>
        	 @yield('content')
		</div>

	    <!-- BEGIN: Vendor JS-->
	    <script src="{{ asset('templates/html/vendors/js/vendors.min.js') }}"></script>
	    <!-- BEGIN Vendor JS-->

	    <!-- BEGIN: Page Vendor JS-->
	    <script src="{{ asset('templates/html/vendors/js/forms/validation/jquery.validate.min.js') }}"></script>
	    <!-- END: Page Vendor JS-->

	    <!-- BEGIN: Theme JS-->
	    <script src="{{ asset('templates/html/js/core/app-menu.js') }}"></script>
	    <script src="{{ asset('templates/html/js/core/app.js') }}"></script>
	    <!-- END: Theme JS-->

	    <!-- BEGIN: Page JS-->
	    <script src="{{ asset('templates/html/js/scripts/pages/page-auth-login.js') }}"></script>
	    <!-- END: Page JS-->

	    <script>
	        $(window).on('load', function() {
	            if (feather) {
	                feather.replace({
	                    width: 14,
	                    height: 14
	                });
	            }
	        })
	    </script>
	</body>
	<!-- END: Body-->

</html>