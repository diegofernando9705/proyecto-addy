<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AddyPersona extends Model
{
    use HasFactory;

    protected $fillable = [
		'cedula_persona',
		'tipo_identificacion',
		'foto_persona',
		'nombres_persona',
		'apellidos_persona',
		'fecha_nacimiento',
		'correo_persona',
		'telefono_fijo',
		'celular_movil',
		'celular_whatsapp',
		'direccion_persona',
		'codigo_ciudad',
		'codigo_pais',
		'codigo_postal',
		'nit_empresa',
		'id_perfil',
		'id_usuario',
		'estado_persona',
    ];
}
