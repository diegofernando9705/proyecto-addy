<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AddyRespuesta extends Model
{
    use HasFactory;

    protected $fillable = [
		'id',
		'descripcion_respuesta',
		'id_pregunta',
		'id_formulario',
		'fecha_respuesta',
		'estado_respuesta',
    ];
    
}
