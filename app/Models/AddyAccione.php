<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AddyAccione extends Model
{
    use HasFactory;

    protected $fillable = [
		'codigo_bitacora',
		'descripcion_bitacora',
		'fecha_bitacora',
		'id_creador',
		'id_accion',
		'id_estado',
    ];
}
