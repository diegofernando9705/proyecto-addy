<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AddyFormulario extends Model
{
    use HasFactory;

    protected $fillable = [
		'pregunta',
		'respuesta',
		'fecha_respuesta',
		'cedula_persona',
		'tipo_cliente',
		'estado_formulario',
    ];

}
