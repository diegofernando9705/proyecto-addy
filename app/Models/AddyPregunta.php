<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AddyPregunta extends Model
{
    use HasFactory;

    protected $fillable = [
		'id',
		'descripcion_preguntas',
		'id_modulo',
		'id_opcional',
		'id_empresa',
		'fecha_pregunta',
		'nombre_usuario',
		'estado_pregunta',
    ];
}
