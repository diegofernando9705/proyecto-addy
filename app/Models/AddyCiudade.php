<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AddyCiudade extends Model
{
    use HasFactory;

    protected $fillable = [
		'codigo_pais',
		'nombre_ciudad',
    ];
}
