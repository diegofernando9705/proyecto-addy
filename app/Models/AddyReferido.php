<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AddyReferido extends Model
{
    use HasFactory;

    protected $fillable = [
		'codigo_pais',
		'nombre_ciudad',
    ];
}
