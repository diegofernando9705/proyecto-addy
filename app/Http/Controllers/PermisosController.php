<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\AddyEstado;
use App\Permisos\Models\Role;
use App\Permisos\Models\Permission AS Permisos;
use DB;

class PermisosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        return view('pages.permisos.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $code_permisos = [];

        $permisos_rol = DB::table('permission_role')
                    ->select('permission_role.*', 'roles.*', 'permissions.id AS permiso_id', 'permissions.name AS permiso_nombre', 'permissions.slug AS permiso_slug', 'permissions.description', 'permissions.name_module')
                    ->join('roles', 'permission_role.role_id', '=', 'roles.id')
                    ->join('permissions', 'permission_role.permission_id', '=', 'permissions.id')
                    ->where('permission_role.role_id', '=', $id)
                    ->get();
        $permisos = Permisos::all();

        foreach ($permisos_rol as $value) {
            $code_permisos[] = $value->permiso_id;
        }

        return view('pages.configuracion.roles.asignacion-permisos', compact('permisos_rol', 'permisos', 'code_permisos', 'id'));
    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        DB::table('permission_role')->where('role_id', $id)->delete();
        foreach ($request->permisos as $value) {
            DB::table('permission_role')->insert([
                'role_id'   => $id,
                'permission_id' => $value
            ]);
        }

        return redirect()->route('roles.index')->with('status_success', 'Se ha actualiado correctamente');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
