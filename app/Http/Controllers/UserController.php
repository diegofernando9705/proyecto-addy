<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Permisos\Models\Role;
use App\Models\Empresa;
use Illuminate\Support\Facades\Hash;
use Storage;
use File;
use DB;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $usuarios = DB::table('users')
                    ->select('empresas.*', 'users.*')
                    ->join('empresas', 'users.id_empresa', '=', 'empresas.id')
                    ->get();

        return view("pages.configuracion.usuarios.index", compact("usuarios"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $empresas = Empresa::all();
        $roles = Role::all();

        return view("pages.configuracion.usuarios.crear", compact("empresas", "roles"));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [          
            'avatar'              => 'required|mimes:jpg,jpeg,bmp,png|max:5000',
            'nombre_usuario'      => 'required',
            'correo_usuario'      => 'required|unique:users,email',
            'password'            => 'required',
            'rol'                 => 'required',
            'empresa'             => 'required',
            'estado_usuario'      => 'required'
        ];
        
        $messages = [
            'avatar.required'               => 'El avatar del usuario es importante.',
            'avatar.mimes' => 'El campo logo debe ser un archivo de imagen (jpg, jpeg, png)',
            'nombre_usuario.required'       => 'El nombre del usuario es importante.',
            'correo_usuario.required'       => 'El correo del usuario no puede quedar vacio.',
            'password.required'             => 'Escriba una contraseña.',
            'rol.required'                  => 'Seleccione rol del usuario.',
            'empresa.in'                    => 'Por favor, seleccione una empresa.',
            'estado_usuario.required'       => 'Por favor, seleccione el estado del usuario.',
        ];

        $this->validate($request, $rules, $messages);

        if($request->password != $request->password_confirmation){
             return back()->with('errors','Datos no coiciden');
        }

        if($request->correo_usuario != $request->correo_repeat){
             return back()->with('errors','Datos no coiciden');
        }

        $directorio = 'empresas/' . $request->empresa;
        
        if (!File::exists($directorio)) {
            Storage::makeDirectory($directorio);
        }
        
        $logo = $request->file('avatar')->store($directorio);

        User::create([
            'name'          => $request->nombre_usuario,
            'email'         => $request->correo_usuario,
            'password'      => Hash::make($request->password),
            'avatar'        => $logo,
            'id_facebook'   => '1',
            'id_google'     => '1',
            'id_empresa'    => $request->empresa,
            'estado_user'   => $request->estado_usuario,
        ]);

        $usuario = User::all()->last();

        DB::table('role_user')->insert([
            'role_id' => $request->rol,
            'user_id'   => $usuario->id
        ]);
        
        return redirect()->route('users.index')->with('status_success', 'Se ha registrado el usuario en nuestro sistema.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $usuario = DB::table('role_user')
                    ->select('role_user.*', 'roles.*', 'users.id AS usuario_id', 'users.name', 'users.email', 'users.avatar')
                    ->join('roles', 'role_user.role_id', '=', 'roles.id')
                    ->join('users', 'role_user.user_id', '=', 'users.id')
                    ->where('users.id', '=', $id)
                    ->get();
        
        return view('pages.configuracion.usuarios.show', compact('usuario'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return $id;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
