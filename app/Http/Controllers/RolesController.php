<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Permisos\Models\Role;
use App\Permisos\Models\Permission AS Permisos;
use App\Models\AddyModulo AS Modulos;
use App\Models\Empresa;
use Gate;
use DB;

class RolesController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        
        Gate::authorize('haveaccess','permisos.habilitado.roles');
        
        $config = ["pageHeader" => false];
        $roles = Role::all();
        return view('pages.configuracion.roles.index', ['pageConfig' => $config], compact('roles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {

        Gate::authorize('haveaccess','permisos.crear.roles');

        $empresa_unica = Empresa::where('id', auth()->user()->id_empresa)->get();

        $empresas = Empresa::all();
        $permisos = Permisos::all();

        return view('pages.configuracion.roles.create', compact('empresas', 'permisos', 'empresa_unica'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {

        Gate::authorize('haveaccess','permisos.crear.roles');

        $rules = [
            'nombre_rol'        => 'required|unique:roles,nombre',
            'slug_rol'          => 'required|unique:roles,slug',
            'descripcion_rol'   => 'required',
            'acceso_total'      => 'required|in:yes,no',
            'estado_rol'        => 'required',
        ];
        
        $messages = [
            'nombre_rol.required'       => 'El nombre del Rol es importante.',
            'slug_rol.required'         => 'El slug no puede quedar vacio.',
            'descripcion_rol.required'  => 'Escriba una descripciòn del Rol a crear',
            'acceso_total.required'     => 'Es importante que nos de una opciòn.',
            'acceso_total.in'           => 'Por favor, seleccione -Si- o -No-.',
            'estado_rol.required'       => 'Por favor, seleccione el estado del Rol.',
        ];

        $this->validate($request, $rules, $messages);

        $empresa_unica = Empresa::where('id', auth()->user()->id_empresa)->get();

        if($request->acceso_total == 'no'){
            $acceso_total = 'no';

            $rol = Role::create([
                'nombre'        => $request->nombre_rol,
                'slug'          => $empresa_unica[0]->indentificacion_tributaria . "." . $request->slug_rol,
                'descripcion'   => $request->descripcion_rol,
                'estado_rol'    => $request->estado_rol,
                'full-access'   => $acceso_total
            ]);

        }else{
            $acceso_total = 'si';

            $rol = Role::create([
                'nombre'        => $request->nombre_rol,
                'slug'          => $empresa_unica[0]->indentificacion_tributaria . "." . $request->slug_rol,
                'descripcion'   => $request->descripcion_rol,
                'estado_rol'    => $request->estado_rol,
                'full-access'   => $acceso_total
            ]);
        }


        foreach ($request->permission as $value) {
            DB::table('permission_role')->insert([
                'role_id'       => $rol->id,
                'permission_id' => $value
            ]);
        }

        if (isset($request->empresas)) {
            $rol = Role::all()->last();

            foreach ($request->empresas as $empresa) {
                DB::table('addy_detalle_roles_empresas')->insert([
                    'empresa_id' => $empresa,
                    'role_id' => $rol->id
                ]);
            }
        }
        
        return true;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {

        Gate::authorize('haveaccess','permisos.ver.roles');
        
        $rol = DB::table('addy_detalle_roles_empresas')
                ->select('addy_detalle_roles_empresas.*', 'roles.*', 'empresas.id', 'empresas.nombre_empresa')
                ->join('roles', 'addy_detalle_roles_empresas.role_id', '=', 'roles.id')
                ->join('empresas', 'addy_detalle_roles_empresas.empresa_id', '=', 'empresas.id')
                ->where('addy_detalle_roles_empresas.role_id', $id)
                ->get();


        return view('pages.configuracion.roles.show', compact('rol'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {

        Gate::authorize('haveaccess','permisos.editar.roles');

        $code_empresa = [];

        $rol = DB::table('addy_detalle_roles_empresas')
                ->select('addy_detalle_roles_empresas.*', 'roles.*', 'empresas.id AS codigo_empresa', 'empresas.nombre_empresa')
                ->join('roles', 'addy_detalle_roles_empresas.role_id', '=', 'roles.id')
                ->join('empresas', 'addy_detalle_roles_empresas.empresa_id', '=', 'empresas.id')
                ->where('addy_detalle_roles_empresas.role_id', $id)
                ->get();

        $empresas = Empresa::all();
        $roles = Role::all();

        foreach ($rol as $value) {
            $code_empresa[] = $value->codigo_empresa;
        }

        return view('pages.configuracion.roles.edit', compact('rol', 'empresas', 'roles', 'code_empresa'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        Gate::authorize('haveaccess','roles.edit');
        return "Hola";
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        Gate::authorize('haveaccess','roles.delete');
    }
}
