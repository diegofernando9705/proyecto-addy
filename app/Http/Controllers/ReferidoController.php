<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\AddyPregunta     AS Preguntas;
use App\Models\AddyRespuesta    AS Respuestas;
use App\Models\AddyPersona      AS Personas;
use App\Models\AddyPaise        AS Paises;
use App\Models\AddyCiudade      AS Ciudades;
use App\Models\AddyFormulario   AS Formularios;

use Gate;
use File;
use Storage;
use DB;

class ReferidoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        Gate::authorize('haveaccess','permisos.habilitado.referidos');
        
    
        $informacion = DB::table('role_user')
        ->select('role_user.role_id','role_user.user_id', 'users.id', 'users.name', 'roles.id', 'roles.nombre', 'roles.slug')
        ->join('users', 'role_user.user_id', '=', 'users.id')
        ->join('roles', 'role_user.role_id', '=', 'roles.id')
        ->where('user_id', auth()->user()->id)
        ->get();
        
        $usuarios_creadores = [];
        $acciones_bitacora = [];
        $estados_bitacora = [];

        if($informacion[0]->slug == 'administrator'){
            $referidos = Personas::where('id_perfil', 3)->get();
        }else{
            $persona = Personas::where('id_usuario', auth()->user()->id)->get();
            $referidos = Personas::where('nit_empresa', $persona[0]->cedula_persona)->get();
        }

        return view('pages.referidos.index', compact('referidos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(auth()->user()->estado_user == 3){
            
        }else{
            $paises = Paises::all();
            return view('pages.referidos.create', compact('paises'));
            
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        Gate::authorize('haveaccess','permisos.crear.referidos');

        $rules = [
            'numero_cedula'     => 'numeric|required|unique:addy_personas,cedula_persona',
            'nombres'           => 'required',
            'apellidos'         => 'required',
            'correo'            => 'required|email|unique:addy_personas,correo_persona',
            'numero_whatsapp'   => 'required|unique:addy_personas,celular_whatsapp',
            'fecha_nacimiento'  => 'required',
            'codigo_pais'       => 'required',
            'codigo_ciudad'     => 'required',
            'respuesta_1'        => 'required',
            'respuesta_2'        => 'required',
            'respuesta_3'        => 'required',
            'respuesta_4'        => 'required',
            'respuesta_5'        => 'required',
            'respuesta_6'        => 'required',
            'respuesta_7'        => 'required',
            'respuesta_8'        => 'required',
            'respuesta_9'        => 'required',
            'respuesta_10'       => 'required',
            'respuesta_11'       => 'required',
            'respuesta_12'       => 'required',
            'respuesta_13'       => 'required',
            'respuesta_14'       => 'required',
            'respuesta_15'       => 'required',
            'respuesta_16'       => 'required',
            'respuesta_17'       => 'required',
            'respuesta_18'       => 'required',
            'respuesta_19'       => 'required',
        ];
        
        $messages = [
            'numero_cedula.required'    => 'El número de cédula es importante.',
            'numero_cedula.unique'      => 'El número de cédula ya se encuentra registrado.',
            'nombres.required'          => 'Registra un nombre.',
            'apellidos.required'        => 'Registra un apellido.',
            'correo.required'           => 'Digite su correo electrónico.',
            'numero_whatsapp.required'  => 'Digite su número de WhatsApp.',
            'numero_whatsapp.unique'    => 'El número de WhatsApp ya se encuentre registrado.',
            'fecha_nacimiento.required' => 'Coloque su fecha de nacimiento.',
            'codigo_pais.required'      => 'Es importante un País.',
            'codigo_ciudad.required'    => 'Seleccione una ciudad.',
            'tipo_cliente.required'     => 'Por favor, seleccione un tipo de cliente.',
            'respuesta_1.required'       => 'La pregunta 1 no puede quedar en blanco.',
            'respuesta_2.required'       => 'La pregunta 2 no puede quedar en blanco.',
            'respuesta_3.required'       => 'La pregunta 3 no puede quedar en blanco.',
            'respuesta_4.required'       => 'La pregunta 4 no puede quedar en blanco.',
            'respuesta_5.required'       => 'La pregunta 5 no puede quedar en blanco.',
            'respuesta_6.required'       => 'La pregunta 6 no puede quedar en blanco.',
            'respuesta_7.required'       => 'La pregunta 7 no puede quedar en blanco.',
            'respuesta_8.required'       => 'La pregunta 8 no puede quedar en blanco.',
            'respuesta_9.required'       => 'La pregunta 9 no puede quedar en blanco.',
            'respuesta_10.required'      => 'La pregunta 10 no puede quedar en blanco.',
            'respuesta_11.required'      => 'La pregunta 11 no puede quedar en blanco.',
            'respuesta_12.required'      => 'La pregunta 12 no puede quedar en blanco.',
            'respuesta_13.required'      => 'La pregunta 13 no puede quedar en blanco.',
            'respuesta_14.required'      => 'La pregunta 14 no puede quedar en blanco.',
            'respuesta_15.required'      => 'La pregunta 15 no puede quedar en blanco.',
            'respuesta_16.required'      => 'La pregunta 16 no puede quedar en blanco.',
            'respuesta_17.required'      => 'La pregunta 17 no puede quedar en blanco.',
            'respuesta_18.required'      => 'La pregunta 18 no puede quedar en blanco.',
            'respuesta_19.required'      => 'La pregunta 19 no puede quedar en blanco.',
        ];

        $this->validate($request, $rules, $messages);

        date_default_timezone_set('America/Bogota');

        $i=1;

        $respuesta1 = $request->respuesta_1;
        $respuesta2 = $request->respuesta_2;
        $respuesta3 = $request->respuesta_3;
        $respuesta4 = $request->respuesta_4;
        $respuesta5 = $request->respuesta_5;
        $respuesta6 = $request->respuesta_6;
        $respuesta7 = $request->respuesta_7;
        $respuesta8 = $request->respuesta_8;
        $respuesta9 = $request->respuesta_9;
        $respuesta10 = $request->respuesta_10;
        $respuesta11 = $request->respuesta_11;
        $respuesta12 = $request->respuesta_12;
        $respuesta13 = $request->respuesta_13;
        $respuesta14 = $request->respuesta_14;
        $respuesta15 = $request->respuesta_15;
        $respuesta16 = $request->respuesta_16;
        $respuesta17 = $request->respuesta_17;
        $respuesta18 = $request->respuesta_18;
        $respuesta19 = $request->respuesta_19;

        Formularios::create([
            'pregunta'  => $request->preguntas[0],
            'respuesta' => $respuesta1,
            'fecha_respuesta'   => date('Y-m-d H:i:s'),
            'cedula_persona'    => $request->numero_cedula,
            'tipo_cliente'      => $request->tipo_cliente,
            'estado_formulario' => '1'
        ]);

        Formularios::create([
            'pregunta'  => $request->preguntas[1],
            'respuesta' => $respuesta2,
            'fecha_respuesta'   => date('Y-m-d H:i:s'),
            'cedula_persona'    => $request->numero_cedula,
            'tipo_cliente'      => $request->tipo_cliente,
            'estado_formulario' => '1'
        ]);

        Formularios::create([
            'pregunta'  => $request->preguntas[2],
            'respuesta' => $respuesta3,
            'fecha_respuesta'   => date('Y-m-d H:i:s'),
            'cedula_persona'    => $request->numero_cedula,
            'tipo_cliente'      => $request->tipo_cliente,
            'estado_formulario' => '1'
        ]);

        Formularios::create([
            'pregunta'  => $request->preguntas[3],
            'respuesta' => $respuesta4,
            'fecha_respuesta'   => date('Y-m-d H:i:s'),
            'cedula_persona'    => $request->numero_cedula,
            'tipo_cliente'      => $request->tipo_cliente,
            'estado_formulario' => '1'
        ]);                 

        Formularios::create([
            'pregunta'  => $request->preguntas[4],
            'respuesta' => $respuesta5,
            'fecha_respuesta'   => date('Y-m-d H:i:s'),
            'cedula_persona'    => $request->numero_cedula,
            'tipo_cliente'      => $request->tipo_cliente,
            'estado_formulario' => '1'
        ]);

        Formularios::create([
            'pregunta'  => $request->preguntas[5],
            'respuesta' => $respuesta6,
            'fecha_respuesta'   => date('Y-m-d H:i:s'),
            'cedula_persona'    => $request->numero_cedula,
            'tipo_cliente'      => $request->tipo_cliente,
            'estado_formulario' => '1'
        ]);

        Formularios::create([
            'pregunta'  => $request->preguntas[6],
            'respuesta' => $respuesta7,
            'fecha_respuesta'   => date('Y-m-d H:i:s'),
            'cedula_persona'    => $request->numero_cedula,
            'tipo_cliente'      => $request->tipo_cliente,
            'estado_formulario' => '1'
        ]);

        Formularios::create([
            'pregunta'  => $request->preguntas[7],
            'respuesta' => $respuesta8,
            'fecha_respuesta'   => date('Y-m-d H:i:s'),
            'cedula_persona'    => $request->numero_cedula,
            'tipo_cliente'      => $request->tipo_cliente,
            'estado_formulario' => '1'
        ]);

        Formularios::create([
            'pregunta'  => $request->preguntas[8],
            'respuesta' => $respuesta9,
            'fecha_respuesta'   => date('Y-m-d H:i:s'),
            'cedula_persona'    => $request->numero_cedula,
            'tipo_cliente'      => $request->tipo_cliente,
            'estado_formulario' => '1'
        ]);

        Formularios::create([
            'pregunta'  => $request->preguntas[9],
            'respuesta' => $respuesta10,
            'fecha_respuesta'   => date('Y-m-d H:i:s'),
            'cedula_persona'    => $request->numero_cedula,
            'tipo_cliente'      => $request->tipo_cliente,
            'estado_formulario' => '1'
        ]);

        Formularios::create([
            'pregunta'  => $request->preguntas[10],
            'respuesta' => $respuesta11,
            'fecha_respuesta'   => date('Y-m-d H:i:s'),
            'cedula_persona'    => $request->numero_cedula,
            'tipo_cliente'      => $request->tipo_cliente,
            'estado_formulario' => '1'
        ]);

        Formularios::create([
            'pregunta'  => $request->preguntas[11],
            'respuesta' => $respuesta12,
            'fecha_respuesta'   => date('Y-m-d H:i:s'),
            'cedula_persona'    => $request->numero_cedula,
            'tipo_cliente'      => $request->tipo_cliente,
            'estado_formulario' => '1'
        ]);

        Formularios::create([
            'pregunta'  => $request->preguntas[12],
            'respuesta' => $respuesta13,
            'fecha_respuesta'   => date('Y-m-d H:i:s'),
            'cedula_persona'    => $request->numero_cedula,
            'tipo_cliente'      => $request->tipo_cliente,
            'estado_formulario' => '1'
        ]);

        Formularios::create([
            'pregunta'  => $request->preguntas[13],
            'respuesta' => $respuesta14,
            'fecha_respuesta'   => date('Y-m-d H:i:s'),
            'cedula_persona'    => $request->numero_cedula,
            'tipo_cliente'      => $request->tipo_cliente,
            'estado_formulario' => '1'
        ]);

        Formularios::create([
            'pregunta'  => $request->preguntas[14],
            'respuesta' => $respuesta15,
            'fecha_respuesta'   => date('Y-m-d H:i:s'),
            'cedula_persona'    => $request->numero_cedula,
            'tipo_cliente'      => $request->tipo_cliente,
            'estado_formulario' => '1'
        ]);

        Formularios::create([
            'pregunta'  => $request->preguntas[15],
            'respuesta' => $respuesta16,
            'fecha_respuesta'   => date('Y-m-d H:i:s'),
            'cedula_persona'    => $request->numero_cedula,
            'tipo_cliente'      => $request->tipo_cliente,
            'estado_formulario' => '1'
        ]);

        Formularios::create([
            'pregunta'  => $request->preguntas[16],
            'respuesta' => $respuesta17,
            'fecha_respuesta'   => date('Y-m-d H:i:s'),
            'cedula_persona'    => $request->numero_cedula,
            'tipo_cliente'      => $request->tipo_cliente,
            'estado_formulario' => '1'
        ]);

        Formularios::create([
            'pregunta'  => $request->preguntas[17],
            'respuesta' => $respuesta18,
            'fecha_respuesta'   => date('Y-m-d H:i:s'),
            'cedula_persona'    => $request->numero_cedula,
            'tipo_cliente'      => $request->tipo_cliente,
            'estado_formulario' => '1'
        ]);
        
        Formularios::create([
            'pregunta'  => $request->preguntas[18],
            'respuesta' => $respuesta19,
            'fecha_respuesta'   => date('Y-m-d H:i:s'),
            'cedula_persona'    => $request->numero_cedula,
            'tipo_cliente'      => $request->tipo_cliente,
            'estado_formulario' => '1'
        ]);
        
        $this->validate($request, $rules, $messages);


        
        $directorio = 'personas/' . $request->numero_cedula;
        
        if (!File::exists($directorio)) {
            Storage::makeDirectory($directorio);
        }

        $logo = $request->file('foto_persona')->store($directorio);
    

        $persona = Personas::where('id_usuario', auth()->user()->id)->get();
        
        Personas::create([
            'cedula_persona'        =>  $request->numero_cedula,
            'tipo_identificacion'   =>  "1",
            'foto_persona'          =>  $logo,
            'nombres_persona'       =>  $request->nombres,
            'apellidos_persona'     =>  $request->apellidos,
            'fecha_nacimiento'      =>  $request->fecha_nacimiento,
            'correo_persona'        =>  $request->correo,
            'telefono_fijo'         =>  "1",
            'celular_movil'         =>  $request->numero_whatsapp,
            'celular_whatsapp'      =>  $request->numero_whatsapp,
            'direccion_persona'     =>  "",
            'codigo_ciudad'         =>  $request->codigo_ciudad,
            'codigo_pais'           =>  $request->codigo_pais,
            'codigo_postal'         =>  "1",
            'nit_empresa'           =>  $persona[0]->cedula_persona,
            'id_perfil'             =>  "3",
            'id_usuario'            =>  "0",
            'estado_persona'        =>  "1",
        ]);

        return redirect()->route('referidos.index')->with('status_success', 'Se ha registrado la persona en nuestro sitema');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function tipo_clientes($id){
        
        Gate::authorize('haveaccess','permisos.crear.referidos');

        if(auth()->user()->estado_user == 3){
            return redirect()->route('perfil.usuario');
        }else{

            if(isset($id)){
                if($id != 'cliente_comprador' AND $id != 'cliente_vendedor' AND $id != 'cliente_arrendatario' AND $id != 'cliente_arrendador'){
                    return "<div class='alert alert-danger'>Opción inválida.</div>";
                }else{

                    if($id == 'cliente_comprador'){
                        $paises = Paises::all();

                        return view('pages.referidos.tipo_clientes.cliente_comprador', compact('paises'));
                    }else if($id == 'cliente_vendedor'){
                        
                        $paises = Paises::all();
                        return view('pages.referidos.tipo_clientes.cliente_vendedor', compact('paises'));
                        
                    }else if($id == 'cliente_arrendatario'){
                        return view('pages.referidos.tipo_clientes.cliente_arrendatario');
                    }else if($id == 'cliente_arrendador'){
                        return view('pages.referidos.tipo_clientes.cliente_arrendador');
                    }else{
                        return "<div class='alert alert-danger'>Opción inválida.</div>";
                    }
                }
            }
        }
    }

}
