<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Empresa      AS Empresas;
use App\Models\AddyModulo   AS Modulos;
use App\Models\AddyPregunta AS Preguntas;

use Gate;

class PreguntasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        $empresas   = Empresas::all();
        $modulos    = Modulos::all();
        $preguntas  = Preguntas::all();

        return view('pages.configuracion.preguntas.index', compact('empresas', 'modulos', 'preguntas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $empresas   = Empresas::all();
        $modulos    = Modulos::all();

        return view('pages.configuracion.preguntas.create', compact('empresas', 'modulos'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Gate::authorize('haveaccess','permisos.crear.preguntas');

        $rules = [
            'pregunta'   => 'required',
            'empresas'   => 'required',
            'modulo'     => 'required',
            'estado'     => 'required',
        ];
        
        $messages = [
            'pregunta.required'     => 'La descripción de la pregunta es importante.',
            'empresas.required'     => 'Seleccione una empresa para la pregunta.',
            'modulo.required'       => 'Seleccione un módulo para la pregunta.',
            'estado.required'       => 'El estado para la pregunta es importante.',
        ];

        $this->validate($request, $rules, $messages);
        $fecha = date('Y-m-d');
        Preguntas::create([
            'descripcion_preguntas' => $request->pregunta,
            'id_modulo'             => $request->modulo,
            'id_opcional'           => $request->codigo_opcional,
            'id_empresa'            => $request->empresas,
            'fecha_pregunta'        => $fecha,
            'nombre_usuario'        => auth()->user()->name,
            'estado_pregunta'       => $request->estado,
        ]);

        return redirect()->route('preguntas.create')->with('status_success', 'Se ha registrado la pregunta en nuestro sitema');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        Gate::authorize('haveaccess','permisos.editar.preguntas');

        $pregunta   = Preguntas::where('id', $id)->get();
        $empresas   = Empresas::all();
        $modulos    = Modulos::all();

        return view('pages.configuracion.preguntas.edit', compact('empresas', 'modulos', 'pregunta'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        Gate::authorize('haveaccess','permisos.editar.preguntas');

        $rules = [
            'pregunta'   => 'required',
            'empresas'   => 'required',
            'modulo'     => 'required',
            'estado'     => 'required',
        ];
        
        $messages = [
            'pregunta.required'     => 'La descripción de la pregunta es importante.',
            'empresas.required'     => 'Seleccione una empresa para la pregunta.',
            'modulo.required'       => 'Seleccione un módulo para la pregunta.',
            'estado.required'       => 'El estado para la pregunta es importante.',
        ];

        $this->validate($request, $rules, $messages);

        $pregunta   = Preguntas::where('id', $id)
                        ->update([
                            'descripcion_preguntas' => $request->pregunta,
                            'id_empresa'            => $request->empresas,
                            'id_modulo'             => $request->modulo,
                            'id_opcional'           => $request->codigo_opcional,
                            'estado_pregunta'       => $request->estado,
                        ]);
        
        return true;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
