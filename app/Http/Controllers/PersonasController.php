<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\AddyPersona AS Personas;
use App\Models\Empresa AS Empresas;
use App\Models\AddyPaise AS Paises;
use App\Models\AddyCiudade AS Ciudades;
use App\Models\User as Usuarios;
use Gate;
use Storage;
use File;
use DB;

class PersonasController extends Controller
{   
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        Gate::authorize('haveaccess','permisos.habilitado.personas');

        $informacion = DB::table('role_user')
        ->select('role_user.role_id','role_user.user_id', 'users.id', 'users.name', 'roles.id', 'roles.nombre', 'roles.slug')
        ->join('users', 'role_user.user_id', '=', 'users.id')
        ->join('roles', 'role_user.role_id', '=', 'roles.id')
        ->where('user_id', auth()->user()->id)
        ->get();
        
        $usuarios_creadores = [];
        $acciones_bitacora = [];
        $estados_bitacora = [];

        if($informacion[0]->slug == 'administrator'){
            $personas = Personas::where('id_perfil', 1)->get();
        }else{
            $personas = Personas::where('id_perfil', 1)->get();
        }

        return view('pages.personas.index', compact('personas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        Gate::authorize('haveaccess','permisos.crear.personas');

        $paises = Paises::all();
        $ciudades = Ciudades::all();
        $usuarios = Usuarios::all();
        $empresas = Empresas::all();

        return view('pages.personas.create', compact('paises', 'ciudades', 'usuarios', 'empresas'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        Gate::authorize('haveaccess','permisos.crear.personas');

        $rules = [
            'foto_persona'              => 'mimes:jpg,jpeg,bmp,png|max:5000',
            'code_tipo_documento'       => 'required',
            'cedula_persona'            => 'required|unique:addy_personas,cedula_persona',
            'primer_nombre'             => 'required',
            'primer_apellido'           => 'required',
            'fecha_nacimiento'          => 'required',
            'email'                     => 'required',
            'celular'                   => 'required',
            'whatsApp'                  => 'required',
            'empresa'                   => 'required',
            'codigo_pais'               => 'required',
            'codigo_ciudad'             => 'required',
            'perfil'                    => 'required',
            'usuario'                   => 'required',
            'estado'                    => 'required',
        ];
        
        $messages = [
            'foto_persona.required'         => 'La imagen de la persona es importante.',
            'code_tipo_documento.required'  => 'El Tipo de documento no puede quedar vacio.',
            'cedula_persona.required'       => 'Ingrese un número de cédula.',
            'cedula_persona.unique'         => 'El número de cédula ya se encuentra registrada.',
            'primer_nombre.required'        => 'Digite su primer nombre.',
            'primer_apellido.required'      => 'Digite el primer apellido.',
            'fecha_nacimiento.required'     => 'Digite fecha de nacimiento.',
            'email.required'                => 'El Email es necesario.',
            'celular.required'              => 'Coloque un celular.',
            'whatsApp.required'             => 'El número de WhatsApp es importante.',
            'empresa.required'              => 'Por favor, seleccione una empresa.',
            'codigo_pais.required'          => 'Por favor, seleccione un País.',
            'codigo_ciudad.required'        => 'Por favor, seleccione una ciudad.',
            'perfil.required'               => 'Por favor, seleccione el perfil a asignar.',
            'usuario.required'              => 'Por favor, seleccione un usuario a asignar.',
            'estado.required'               => 'Por favor, seleccione el estado de la persona.',
        ];

        $this->validate($request, $rules, $messages);

        
        $directorio = 'personas/' . $request->cedula_persona;
        
        if (!File::exists($directorio)) {
            Storage::makeDirectory($directorio);
        }

        $logo = $request->file('foto_persona')->store($directorio);


        Personas::create([
            'cedula_persona'        =>  $request->cedula_persona,
            'tipo_identificacion'   =>  $request->code_tipo_documento,
            'foto_persona'          =>  $logo,
            'nombres_persona'       =>  $request->primer_nombre." ".$request->segundo_nombre,
            'apellidos_persona'     =>  $request->primer_apellido." ".$request->segundo_apellido,
            'fecha_nacimiento'      =>  $request->fecha_nacimiento,
            'correo_persona'        =>  $request->email,
            'telefono_fijo'         =>  $request->telefono_fijo,
            'celular_movil'         =>  $request->celular,
            'celular_whatsapp'      =>  $request->whatsApp,
            'direccion_persona'     =>  $request->direccion,
            'codigo_ciudad'         =>  $request->codigo_ciudad,
            'codigo_pais'           =>  $request->codigo_pais,
            'codigo_postal'         =>  $request->codigo_postal,
            'nit_empresa'           =>  $request->empresa,
            'id_perfil'             =>  $request->perfil,
            'id_usuario'            =>  $request->usuario,
            'estado_persona'        =>  $request->estado,
        ]);

        return redirect()->route('personas.index')->with('status_success', 'Se ha registrado la persona en nuestro sitema');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function show($id)
    {   

        Gate::authorize('haveaccess','permisos.ver.personas');

        $persona = Personas::where('cedula_persona', '=', $id)->get();

        $pais = Paises::where('codigo', '=', $persona[0]->codigo_pais)->get();
        $ciudad = Ciudades::where('id_ciudades', '=', $persona[0]->codigo_ciudad)->get();
        $usuario = Usuarios::where('id', '=', $persona[0]->id_usuario)->get();
        $empresa = Empresas::where('id', '=', $persona[0]->nit_empresa)->get();

        return view('pages.personas.show', compact('persona', 'pais', 'ciudad', 'usuario', 'empresa'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        Gate::authorize('haveaccess','permisos.editar.personas');

        $persona = Personas::where('cedula_persona', '=', $id)->get();

        $paises = Paises::all();
        $ciudades = Ciudades::all();
        $usuarios = Usuarios::all();  
        $empresas = Empresas::all();

        
        $pais = Paises::where('codigo', '=', $persona[0]->codigo_pais)->get();
        $ciudad = Ciudades::where('id_ciudades', '=', $persona[0]->codigo_ciudad)->get();
        $usuario = Usuarios::where('id', '=', $persona[0]->id_usuario)->get();
        $empresa = Empresas::where('id', '=', $persona[0]->nit_empresa)->get();

        return view('pages.personas.edit', compact('persona', 'pais', 'ciudad', 'usuario', 'empresa', 'paises', 'ciudades', 'usuarios', 'empresas'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {   
        Gate::authorize('haveaccess','permisos.editar.personas');


        $rules = [
            'foto_persona'          => 'mimes:jpg,jpeg,bmp,png|max:5000',
            'code_tipo_documento'   => 'required',
            'cedula_persona'        => 'required',
            'primer_nombre'         => 'required',
            'primer_apellido'       => 'required',
            'email'                 => 'required',
            'celular'               => 'required',
            'whatsApp'              => 'required',
            'empresa'               => 'required',
            'codigo_pais'           => 'required',
            'codigo_ciudad'         => 'required',
            'perfil'                => 'required',
            'usuario'               => 'required',
            'estado'                => 'required',
        ];
        
        $messages = [
            'foto_persona.mimes'            => 'La foto de persona debe ser un archivo de imagen.',
            'code_tipo_documento.required'  => 'El Tipo de documento no puede quedar vacio.',
            'cedula_persona.required'       => 'Ingrese un número de cédula.',
            'primer_nombre.required'        => 'Digite su primer nombre.',
            'primer_apellido.required'      => 'Digite el primer apellido.',
            'email.required'                => 'El Email es necesario.',
            'celular.required'              => 'Coloque un celular.',
            'whatsApp.required'             => 'El número de WhatsApp es importante.',
            'empresa.required'              => 'Por favor, seleccione una empresa.',
            'codigo_pais.required'          => 'Por favor, seleccione un País.',
            'codigo_ciudad.required'        => 'Por favor, seleccione una ciudad.',
            'perfil.required'               => 'Por favor, seleccione el perfil a asignar.',
            'usuario.required'              => 'Por favor, seleccione un usuario a asignar.',
            'estado.required'               => 'Por favor, seleccione el estado de la persona.',
        ];

        $this->validate($request, $rules, $messages);

        if(isset($request->foto_persona)){
            
            $directorio = 'personas/' . $request->cedula_persona;
        
            if (!File::exists($directorio)) {
                Storage::makeDirectory($directorio);
            }

            $logo = $request->file('foto_persona')->store($directorio);
            
        }else{

            $foto = Personas::where('cedula_persona', $id)->get();
            
            $logo = $foto[0]->foto_persona;
        }

        Personas::where('cedula_persona', $id)
                ->update([
                    'tipo_identificacion'   => $request->code_tipo_documento,
                    'foto_persona'          => $logo,
                    'nombres_persona'       => $request->primer_nombre,
                    'apellidos_persona'     => $request->primer_apellido,
                    'correo_persona'        => $request->email,
                    'telefono_fijo'         => $request->telefono_fijo,
                    'celular_movil'         => $request->celular,
                    'celular_whatsapp'      => $request->whatsApp,
                    'direccion_persona'     => $request->direccion,
                    'codigo_ciudad'         => $request->codigo_ciudad,
                    'codigo_pais'           => $request->codigo_pais,
                    'codigo_postal'         => $request->codigo_postal,
                    'nit_empresa'           => $request->empresa,
                    'id_perfil'             => $request->perfil,
                    'id_usuario'            => $request->usuario,
                    'estado_persona'        => $request->estado
        ]);


        return redirect()->route('personas.index')->with('status_success', 'Se ha actualizado la información en nuestro sitema');
    }

    public function perfil(){

        $usuario = Usuarios::where('id', auth()->user()->id)->get();
        
        if($usuario[0]->email == 'admin@admin.com'){
            return true;
        }else{

            if(auth()->user()->estado_user == 3){
                return "sin_registro";
            }else if(auth()->user()->estado_user == 1){
                return view('pages.configuracion.usuarios.perfil_actualizado');
            }else{

            }    
        }
    }


    public function perfilUnico(){



        $id = auth()->user()->id;
        

        if(auth()->user()->estado_user == '1'){
            
            return redirect()->route('home')->with('status_success', 'Se ha actualizado la información en nuestro sitema');

        }else{

            $usuario = Usuarios::where('id', $id)->get();

            $paises = Paises::all();
            $ciudades = Ciudades::all();
            $usuarios = Usuarios::all();  
            $empresas = Empresas::all();

            return view('pages.configuracion.usuarios.perfil', compact('paises', 'ciudades', 'usuarios', 'empresas'));
            
        }

        
    }

    public function perfilStore(Request $request){

        $rules = [
            'foto_persona'              => 'required|mimes:jpg,jpeg,bmp,png|max:5000',
            'code_tipo_documento'       => 'required',
            'cedula_persona'            => 'required|unique:addy_personas,cedula_persona',
            'primer_nombre'             => 'required',
            'primer_apellido'           => 'required',
            'fecha_nacimiento'          => 'required',
            'email'                     => 'required',
            'celular'                   => 'required',
            'whatsApp'                  => 'required',
            'empresa'                   => 'required',
            'codigo_pais'               => 'required',
            'codigo_ciudad'             => 'required',
            'perfil'                    => 'required',
            'usuario'                   => 'required',
            'estado'                    => 'required',
        ];
        
        $messages = [
            'foto_persona.required'         => 'La imagen de la persona es importante.',
            'foto_persona.mimes'            => 'La foto de persona debe ser un archivo de imagen.',
            'code_tipo_documento.required'  => 'El Tipo de documento no puede quedar vacio.',
            'cedula_persona.required'       => 'Ingrese un número de cédula.',
            'cedula_persona.unique'         => 'El número de cédula ya se encuentra registrada.',
            'primer_nombre.required'        => 'Digite su primer nombre.',
            'primer_apellido.required'      => 'Digite el primer apellido.',
            'fecha_nacimiento.required'     => 'Digite fecha de nacimiento.',
            'email.required'                => 'El Email es necesario.',
            'celular.required'              => 'Coloque un celular.',
            'whatsApp.required'             => 'El número de WhatsApp es importante.',
            'empresa.required'              => 'Por favor, seleccione una empresa.',
            'codigo_pais.required'          => 'Por favor, seleccione un País.',
            'codigo_ciudad.required'        => 'Por favor, seleccione una ciudad.',
            'perfil.required'               => 'Por favor, seleccione el perfil a asignar.',
            'usuario.required'              => 'Por favor, seleccione un usuario a asignar.',
            'estado.required'               => 'Por favor, seleccione el estado de la persona.',
        ];

        $this->validate($request, $rules, $messages);

        $directorio = 'personas/' . $request->cedula_persona;
        
        if (!File::exists($directorio)) {
            Storage::makeDirectory($directorio);
        }

        $logo = $request->file('foto_persona')->store($directorio);


        Personas::create([
            'cedula_persona'        =>  $request->cedula_persona,
            'tipo_identificacion'   =>  $request->code_tipo_documento,
            'foto_persona'          =>  $logo,
            'nombres_persona'       =>  $request->primer_nombre." ".$request->segundo_nombre,
            'apellidos_persona'     =>  $request->primer_apellido." ".$request->segundo_apellido,
            'fecha_nacimiento'      =>  $request->fecha_nacimiento,
            'correo_persona'        =>  $request->email,
            'telefono_fijo'         =>  $request->telefono_fijo,
            'celular_movil'         =>  $request->celular,
            'celular_whatsapp'      =>  $request->whatsApp,
            'direccion_persona'     =>  $request->direccion,
            'codigo_ciudad'         =>  $request->codigo_ciudad,
            'codigo_pais'           =>  $request->codigo_pais,
            'codigo_postal'         =>  $request->codigo_postal,
            'nit_empresa'           =>  $request->empresa,
            'id_perfil'             =>  $request->perfil,
            'id_usuario'            =>  $request->usuario,
            'estado_persona'        =>  $request->estado,
        ]);
        
        Usuarios::where('id', $request->usuario)->update([
            'estado_user' => '1'
        ]);

        DB::table('role_user')->insert([
            'role_id'   => '2',
            'user_id'   => $request->usuario,
        ]);

        return redirect()->route('perfil')->with('status_success', 'Se ha actualizado la información en nuestro sitema');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
