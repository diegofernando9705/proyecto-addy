<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\AddyModulo AS Modulos;
use App\Models\Empresa;
use App\Permisos\Models\Permission AS Permisos;
use App\Permisos\Models\Role;
use DB;
use Illuminate\Support\Facades\Crypt;

class ModulosController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {

        $modulos = Modulos::all();

        return view('pages.modulos.index', compact('modulos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $empresas = Empresa::all();
        return view('pages.modulos.create', compact('empresas'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {

        $rules = [
            'icon_modulo'       => 'required',
            'nombre_modulo'     => 'required',
            'slug_modulo'       => 'required|unique:addy_modulos,slug_modulo',
            'seccion_modulo'    => 'required',
            'estado_modulo'     => 'required',
        ];


        $messages = [
            'icon_modulo.required'      => 'Agregue un icono del módulo.',
            'nombre_modulo.required'    => 'Debes especificar el nombre del módulo.',
            'slug_modulo.required'      => 'El slug del módulo es importante.',
            'slug_modulo.unique'        => 'El slug del Modulo ya se encuentra registrado, verifica e intenta nuevamente.',
            'seccion_modulo.required'   => 'Por favor, seleccione la sección del módulo.',
            'estado_modulo.required'    => 'Seleccione el estado del Módulo a crear.',
        ];

        $this->validate($request, $rules, $messages);


        Modulos::create([
            'icon_modulo' => $request->icon_modulo,
            'nombre_modulo' => $request->nombre_modulo,
            'descripcion_modulo' => $request->descripcion_modulo,
            'slug_modulo' => $request->slug_modulo,
            'seccion_id' => $request->seccion_modulo,
            'estado_id' => $request->estado_modulo,
        ]);

        if($request->permisos){
            foreach ($request->permisos as $value) {
               Permisos::create([
                    'name'          => $value." ".strtolower($request->slug_modulo),
                    'slug'          => "permisos.".$value.".".strtolower($request->slug_modulo),
                    'description'   => "El usuario podra ".$value." ".strtolower($request->slug_modulo),
                    'name_module'   => strtolower($request->slug_modulo)
                ]);
            }
        }

        if ($request->empresa_modulo) {

            $info_modulo = Modulos::all()->last();

            foreach ($request->empresa_modulo as $empresa) {
                DB::table('addy_detalle_modulos_empresas')->insert([
                    'empresa_id'    => $empresa,
                    'modulo_id'     => $info_modulo['id_modulo']
                ]);
            }

            return redirect()->route('modulos.index')->with('status_success', 'Se ha registrado el módulo en nuestro sistema.');
        } else {
            return "NO HAY";
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        $modulo = Modulos::where('id_modulo', $id)->get();

        return view("pages.modulos.show", compact('modulo'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {

        $empresas_del_modulo = [];

        $empresas = Empresa::all();
        $modulo = Modulos::where('id_modulo', $id)->get();

        $modulos_empresas = DB::table('addy_detalle_modulos_empresas')
                ->select('addy_detalle_modulos_empresas.*', 'addy_modulos.*', 'empresas.id AS id_empresa')
                ->join('addy_modulos', 'addy_detalle_modulos_empresas.modulo_id', '=', 'addy_modulos.id_modulo')
                ->join('empresas', 'addy_detalle_modulos_empresas.empresa_id', '=', 'empresas.id')
                ->where('id_modulo', $id)
                ->get();

        foreach ($modulos_empresas as $mod) {
            $empresas_del_modulo[] = $mod->empresa_id;
        }

        return view("pages.modulos.edit", compact('modulo', 'empresas', 'empresas_del_modulo'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        
        $rules = [
            'icon_modulo'       => 'required',
            'nombre_modulo'     => 'required',
            'seccion_modulo'    => 'required',
            'estado_modulo'     => 'required',
        ];
        
        $messages = [
            'icon_modulo.required'      => 'Agregue un icono del módulo.',
            'nombre_modulo.required'    => 'Debes especificar el nombre del módulo.',
            'seccion_modulo.required'   => 'Por favor, seleccione la sección del módulo.',
            'estado_modulo.required'    => 'Seleccione el estado del Módulo a crear.',
        ];
        
        $this->validate($request, $rules, $messages);
        
        $resultado = Modulos::where('slug_modulo', $request->slug_modulo)->count();
        
        Modulos::where('id_modulo', $id)
                ->update([
                    'icon_modulo'           => $request->icon_modulo,
                    'nombre_modulo'         => $request->nombre_modulo,
                    'descripcion_modulo'    => $request->descripcion_modulo,
                    'seccion_id'            => $request->seccion_modulo,
                    'estado_id'             => $request->estado_modulo
        ]);

        DB::table('addy_detalle_modulos_empresas')->where('modulo_id', '=', $id)->delete();

        foreach ($request->empresa_modulo as $empresa) {
            DB::table('addy_detalle_modulos_empresas')->insert([
                'empresa_id'    => $empresa,
                'modulo_id'     => $id
            ]);
        }

        return true;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        //
    }


    public function generar_permisos(Request $request){

    }
}
