<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\AddyPaise AS Paises;
use App\Models\AddyCiudade AS Ciudades;
use App\Models\Empresa;
use Illuminate\Support\Facades\Crypt;
use Storage;
use File;

class EmpresasController extends Controller {

    public function index() {
        $empresas = Empresa::all();
        return view('pages.empresas.index', compact('empresas'));
    }

    public function create() {
        $paises = Paises::all();
        return view('pages.empresas.create', compact('paises'));
    }

    public function store(Request $request) {

        $rules = [
            'logo_empresa' => 'required|mimes:jpg,jpeg,bmp,png|max:5000',
            'favicon_empresa' => 'mimes:jpg,jpeg,png|max:5000',
            'code_tipo_documento' => 'required|integer',
            'indentificacion_tributaria' => 'required|integer|unique:empresas,indentificacion_tributaria',
            'nombre_empresa' => 'required',
            'email_empresa' => 'required|email|unique:empresas,email_empresa',
            'descripcion_empresa' => 'required',
            'telefono_whatsapp' => 'required',
            'codigo_pais' => 'required',
            'codigo_ciudad' => 'required',
            'direccion_empresa' => 'required',
            'estado_empresa' => 'required|integer',
        ];


        $messages = [
            'logo_empresa.required' => 'Agregue un logo de la empresa',
            'logo_empresa.mimes' => 'El campo logo debe ser un archivo de imagen (jpg, jpeg, png)',
            'code_tipo_documento.required' => 'Seleccione un tipo de documento.',
            'indentificacion_tributaria.required' => 'Por favor, digite el número de su NIT/CC',
            'indentificacion_tributaria.unique' => 'La identificación de la empresa ya se encuentra registrada.',
            'nombre_empresa.required' => 'El nombre de la empresa es obligatorio.',
            'email_empresa.required' => 'El correo electrónico es obligatorio.',
            'email_empresa.email' => 'El correo electrónico es inválido.',
            'descripcion_empresa.required' => 'La descripción de la empresa es obligatoria.',
            'telefono_whatsapp.required' => 'El número de whatsapp es obligatorio.',
            'codigo_pais.required' => 'El campo País es obligatorio.',
            'codigo_ciudad.required' => 'El campo Ciudad es obligatorio.',
            'direccion_empresa.required' => 'Proporcione una dirección de la empresa.',
            'estado_empresa.required' => 'El estado de la empresa es obligatorio.',
        ];

        $this->validate($request, $rules, $messages);

        $directorio = 'empresas/' . $request->indentificacion_tributaria;
        
        if (!File::exists($directorio)) {
            Storage::makeDirectory($directorio);
        }
        
        $logo = $request->file('logo_empresa')->store($directorio);
        
        if (empty($request->favicon_empresa)) {
            $favicon = '';
        } else {
            /* carga del logo al proyecto */
            $favicon = $request->file('favicon_empresa')->store($directorio);
            /* FIn carga del logo al proyecto */
        }
        
        $palabras_claves = implode(",", $request->palabras_claves);
        
        Empresa::create([
            'logo_empresa' => $logo,
            'favicon_empresa' => $favicon,
            'code_tipo_documento' => $request->code_tipo_documento,
            'indentificacion_tributaria' => $request->indentificacion_tributaria,
            'nombre_empresa' => $request->nombre_empresa,
            'email_empresa' => $request->email_empresa,
            'descripcion_empresa' => $request->descripcion_empresa,
            'telefono_principal' => $request->telefono_fijo,
            'telefono_whatsapp' => Crypt::encryptString($request->telefono_whatsapp),
            'palabras_claves' => $palabras_claves,
            'acerca_de_negocio' => $request->acerca_negocio,
            'codigo_pais' => $request->codigo_pais,
            'codigo_departamento' => $request->codigo_ciudad,
            'codigo_ciudad' => $request->codigo_ciudad,
            'codigo_postal' => $request->codigo_postal,
            'direccion_empresa' => $request->direccion_empresa,
            'estado_empresa' => $request->estado_empresa
        ]);

        return redirect()->route('empresas.index')->with('status_success', 'Se ha registrado la empresa en nuestro sitema');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {

        $empresa = Empresa::where('id', '=', $id)->get();

        $pais = Paises::where('codigo', '=', $empresa[0]->codigo_pais)->get();
        $ciudad = Ciudades::where('id_ciudades', $empresa[0]->codigo_ciudad)->get();
        $palabras_claves = explode(',', $empresa[0]->palabras_claves);


        return view('pages.empresas.show', compact('empresa', 'pais', 'ciudad', 'palabras_claves'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $empresa = Empresa::where('id', '=', $id)->get();

        $paises = Paises::all();
        $ciudad = Ciudades::where('id_ciudades', $empresa[0]->codigo_ciudad)->get();
        $palabras_claves = explode(',', $empresa[0]->palabras_claves);

        return view('pages.empresas.edit', compact('empresa', 'paises', 'ciudad', 'palabras_claves'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {

        $rules = [
            'code_tipo_documento' => 'required|integer',
            'indentificacion_tributaria' => 'required',
            'nombre_empresa' => 'required',
            'email_empresa' => 'required|email',
            'descripcion_empresa' => 'required',
            'telefono_whatsapp' => 'required',
            'codigo_pais' => 'required',
            'codigo_ciudad' => 'required',
            'direccion_empresa' => 'required',
            'estado_empresa' => 'required|integer',
        ];


        $messages = [
            'code_tipo_documento.required' => 'Seleccione un tipo de documento.',
            'indentificacion_tributaria.required' => 'Por favor, digite el número de su NIT/CC',
            'indentificacion_tributaria.unique' => 'La identificación de la empresa ya se encuentra registrada.',
            'nombre_empresa.required' => 'El nombre de la empresa es obligatorio.',
            'email_empresa.required' => 'El correo electrónico es obligatorio.',
            'email_empresa.email' => 'El correo electrónico es inválido.',
            'descripcion_empresa.required' => 'La descripción de la empresa es obligatoria.',
            'telefono_whatsapp.required' => 'El número de whatsapp es obligatorio.',
            'codigo_pais.required' => 'El campo País es obligatorio.',
            'codigo_ciudad.required' => 'El campo Ciudad es obligatorio.',
            'direccion_empresa.required' => 'Proporcione una dirección de la empresa.',
            'estado_empresa.required' => 'El estado de la empresa es obligatorio.',
        ];

        $this->validate($request, $rules, $messages);

        $directorio = 'empresas/' . $id;

        if (!File::exists($directorio)) {
            Storage::makeDirectory($directorio);
        }

        if (empty($request->logo_empresa)) {
            $logo = $request->imagen_bd_logo;
        } else {
            /* carga del logo al proyecto */
            $logo = $request->file('logo_empresa')->store($directorio);
            /* FIn carga del logo al proyecto */
        }

        if (empty($request->favicon_empresa)) {
            $favicon = $request->imagen_fv_bd;
        } else {
            /* carga del logo al proyecto */
            $favicon = $request->file('favicon_empresa')->store($directorio);
            /* FIn carga del logo al proyecto */
        }
        $palabras_claves = implode(",", $request->palabras_claves);
        Empresa::where('indentificacion_tributaria', $id)
                ->update([
                    'logo_empresa' => $logo,
                    'favicon_empresa' => $favicon,
                    'code_tipo_documento' => $request->code_tipo_documento,
                    'nombre_empresa' => $request->nombre_empresa,
                    'email_empresa' => $request->email_empresa,
                    'descripcion_empresa' => $request->descripcion_empresa,
                    'telefono_principal' => $request->telefono_fijo,
                    'telefono_whatsapp' => Crypt::encryptString($request->telefono_whatsapp),
                    'palabras_claves' => $palabras_claves,
                    'acerca_de_negocio' => $request->acerca_negocio,
                    'codigo_pais' => $request->codigo_pais,
                    'codigo_departamento' => $request->codigo_ciudad,
                    'codigo_ciudad' => $request->codigo_ciudad,
                    'codigo_postal' => $request->codigo_postal,
                    'direccion_empresa' => $request->direccion_empresa,
                    'estado_empresa' => $request->estado_empresa
        ]);

        return redirect()->route('empresas.index')->with('status_success', 'Actualización exitosa');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        
    }

    public function ciudad($id) {
        $ciudades = Ciudades::where('codigo_pais', $id)->get();
        return view('pages.empresas.ciudades', compact('ciudades'));
    }

}
