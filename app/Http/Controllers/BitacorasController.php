<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\AddyPersona      as Personas;
use App\Models\AddyEstado       as Estados;
use App\Models\AddyBitacora     as Bitacoras;
use App\Models\User             as Usuarios;
use App\Models\AddyAccione      as Acciones;
use App\Models\AddyHistoriale   as Historiales;
use App\Models\AddyFormulario   as Formularios;


use DB;
use Gate;

class BitacorasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        
        Gate::authorize('haveaccess','permisos.habilitado.bitacoras');


        $informacion = DB::table('role_user')
                        ->select('role_user.role_id','role_user.user_id', 'users.id', 'users.name', 'roles.id', 'roles.nombre', 'roles.slug')
                        ->join('users', 'role_user.user_id', '=', 'users.id')
                        ->join('roles', 'role_user.role_id', '=', 'roles.id')
                        ->where('user_id', auth()->user()->id)
                        ->get();
        
        $usuarios_creadores = [];
        $acciones_bitacora = [];
        $estados_bitacora = [];

        if($informacion[0]->slug == 'administrator'){
            $bitacoras = Bitacoras::all();
        }else{

            $bitacoras = Bitacoras::where('id_creador', auth()->user()->id)->get();
        }

        foreach ($bitacoras as $bitacora) {
            $usuarios_creadores[]   = $bitacoras[0]->id_creador;
            $acciones_bitacora[]    = $bitacoras[0]->id_accion;
            $estados_bitacora[]     = $bitacoras[0]->id_estado;
        }

        $usuarios   = Usuarios::all();
        $acciones   = Acciones::all();
        $estados    = Estados::all();

        return view('pages.referidos.bitacoras.index', compact('bitacoras', 'usuarios' ,'acciones', 'estados', 'usuarios_creadores', 'acciones_bitacora', 'estados_bitacora'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function generar($cedula)
    {
        
        Gate::authorize('haveaccess','permisos.crear.bitacoras');

        $codigo = "AB-".time();
        $estados = Estados::where('modulo_estado', 'bitacoras')->get();
        return view('pages.referidos.bitacoras.crear', compact('codigo', 'estados', 'cedula'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Gate::authorize('haveaccess','permisos.crear.bitacoras');

        $rules = [
            'cedula'                => 'required',
            'code_bitacora'         => 'required',
            'descripcion_bitacora'  => 'required',
            'fecha_bitacora'        => 'required',
            'usuario_creador'       => 'required',
            'estado_bitacora'       => 'required',
        ];
        
        $messages = [
            'cedula.required'        => 'La cédula es importante.',
            'code_bitacora.required'        => 'El código de la Bitacora es importante.',
            'descripcion_bitacora.required' => 'Escriba una descripción de la Bitacora.',
            'fecha_bitacora.required'       => 'Ingrese la fecha de la Bitacora.',
            'usuario_creador.required'      => 'Es importante el usuario creador.',
            'estado_bitacora.required'      => 'Por favor seleccione el estado de la Bitacora.',
        ];

        $this->validate($request, $rules, $messages);

        Bitacoras::create([
            'codigo_bitacora'       => $request->code_bitacora,
            'descripcion_bitacora'  => $request->descripcion_bitacora,
            'fecha_bitacora'        => $request->fecha_bitacora,
            'id_creador'            => $request->usuario_creador,
            'cedula_persona'        => $request->cedula,
            'id_accion'             => '0',
            'id_estado'             => $request->estado_bitacora,
        ]);

        return redirect()->route('bitacoras.index')->with('status_success', 'Se ha registrado en nuestro sitema');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        Gate::authorize('haveaccess','permisos.ver.bitacoras');
        
        $bitacoras = Bitacoras::where('codigo_bitacora', $id)->get();
        
        foreach ($bitacoras as $bitacora) {
            $usuarios_creadores[]   = $bitacoras[0]->id_creador;
            $acciones_bitacora[]    = $bitacoras[0]->id_accion;
            $estados_bitacora[]     = $bitacoras[0]->id_estado;
        }

        $personas   = Personas::all();
        $usuarios   = Usuarios::all();
        $acciones   = Acciones::all();
        $estados    = Estados::where('modulo_estado', 'bitacoras')->get();
        $historiales    = Historiales::where('codigo_bitacora', $id)->orderBy('id', 'DESC')->get();

        return view('pages.referidos.bitacoras.ver', compact('bitacoras', 'usuarios' ,'acciones', 'estados', 'usuarios_creadores', 'acciones_bitacora', 'estados_bitacora', 'personas', 'historiales'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function acciones($valor, $bitacora){
        
        $informacion_bitacora   = Bitacoras::where('codigo_bitacora', $bitacora)->get();
        $informacion_persona    = Personas::where('cedula_persona', $informacion_bitacora[0]->cedula_persona)->get();
        

        if($valor == '1'){
            return view('pages.configuracion.acciones.registrar_llamada', compact('valor','informacion_bitacora' ,'informacion_persona'));
        }else if ($valor == '2') {
            return view('pages.configuracion.acciones.registro_cita_programada', compact('valor','informacion_bitacora' ,'informacion_persona'));
        }else{

        }
    }

    public function registro(Request $request){
        
        


        $rules = [
            'accion_realizada'              => 'required',
            'informacion_bitacora'          => 'required',

        ];
        
        $messages = [
            'accion_realizada.required'     => 'Seleccione un celular válido.',
            'informacion_bitacora.required'     => 'El código de la Bitacora es importante.',
        ];

        $this->validate($request, $rules, $messages);

        $acciones   = Acciones::where('codigo_accion', $request->accion_realizada)->count();


        if($acciones >= '1'){
            
            if($request->accion_realizada == '1'){
                $rules = [
                    'celular'               => 'required',
                    'hora_inicio'           => 'required',
                    'hora_fin'              => 'required',
                    'resultado_llamada'     => 'required',
                    'usuario_registro'      => 'required',
                    'fecha_registro'        => 'required',
                ];
            
                $messages = [
                    'celular.required'              => 'Seleccione un celular válido.',
                    'hora_inicio.required'          => 'La ahora de inicio es importante.',
                    'hora_fin.required'             => 'La hora final es Importante.',
                    'resultado_llamada.required'    => '¿Cual fue el resultado de la llamada?.',
                    'usuario_registro.required'     => 'El usuario es importante.',
                    'fecha_registro.required'       => 'Por favor, no deje vacio la fecha de registro.',
                ];

                $this->validate($request, $rules, $messages);    
                
                date_default_timezone_set('America/Bogota');

                Historiales::create([
                    'descripcion_historial' => 'Se realizo la acción de Regirar llamada al celular/fijo '.$request->celular." la hora de inicio fue: ".$request->hora_inicio." y la hora de fin fue: ".$request->hora_fin." y el resultado de la llamada fue: ".$request->resultado_llamada." con fecha del: ".$request->fecha_registro,
                    'codigo_bitacora'       => $request->informacion_bitacora,
                    'id_usuario'            => auth()->user()->id,
                    'fecha_historial'       => date('Y-m-d H:i'),
                ]);

            }else if($request->accion_realizada == '2'){
                $rules = [
                    'fecha_inicio'               => 'required',
                    'resultado_agenda'           => 'required',
                    'usuario_registro'      => 'required',
                    'fecha_registro'        => 'required',
                ];
            
                $messages = [
                    'fecha_inicio.required'              => 'Seleccione una fecha válida.',
                    'resultado_agenda.required'          => 'Describa el resultado que es importante.',
                    'usuario_registro.required'     => 'El usuario es importante.',
                    'fecha_registro.required'       => 'Por favor, no deje vacio la fecha de registro.',
                ];

                $this->validate($request, $rules, $messages);    
                
                date_default_timezone_set('America/Bogota');

                Historiales::create([
                    'descripcion_historial' => 'Se realizo la acción de Regiro cita programada al referido, la fecha escogida por ambos es el dia: '.$request->fecha_inicio." y el resultado de la agenda fue: ".$request->resultado_llamada." con fecha del: ".$request->fecha_registro,
                    'codigo_bitacora'       => $request->informacion_bitacora,
                    'id_usuario'            => auth()->user()->id,
                    'fecha_historial'       => date('Y-m-d H:i'),
                ]);
            }
            

        }else{
            return "Error accion no permitida";
        }

        return redirect()->route('bitacoras.index')->with('status_success', 'Se ha registrado en nuestro sitema');

        
    }


    public function informacion_referido($cedula){
        $persona = Personas::where('cedula_persona', $cedula)->get();
        $formularios = Formularios::where('cedula_persona', $cedula)->get();

        return view('pages.referidos.bitacoras.informacion_referidos', compact('persona','formularios'));


    }
}
